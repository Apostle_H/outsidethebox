﻿using UnityEngine;

namespace Plugins.Wrappers
{
    [System.Serializable]
    public class SerializableSystemType
    {
        [field: SerializeField] public string Name { get; private set; }
	    [field: SerializeField] public string AssemblyQualifiedName { get; private set; }
        [field: SerializeField] public string AssemblyName { get; private set; }
	
        private System.Type _systemType;	
        public System.Type SystemType
        {
            get 	
            {
                if (_systemType == null)	
                {
                    GetSystemType();
                }
                return _systemType;
            }
        }
	
        public SerializableSystemType( System.Type _SystemType )
        {
            _systemType = _SystemType;
            Name = _SystemType.Name;
            AssemblyQualifiedName = _SystemType.AssemblyQualifiedName;
            AssemblyName = _SystemType.Assembly.FullName;
        }
        
        private void GetSystemType() => _systemType = System.Type.GetType(AssemblyQualifiedName);
	
        public override bool Equals( System.Object obj )
        {
            var temp = obj as SerializableSystemType;
            return (object)temp != null && this.Equals(temp);
        }
	
        public bool Equals( SerializableSystemType _Object )
        {
            //return m_AssemblyQualifiedName.Equals(_Object.m_AssemblyQualifiedName);
            return _Object.SystemType.Equals(SystemType);
        }
        
        public override int GetHashCode() => SystemType != null ? SystemType.GetHashCode() : 0;

        public static bool operator ==( SerializableSystemType a, SerializableSystemType b )
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
                return true;
            // If one is null, but not both, return false.
            if ((object)a == null || (object)b == null)
                return false;
	
            return a.Equals(b);
        }
	
        public static bool operator !=( SerializableSystemType a, SerializableSystemType b ) => !(a == b);
    }
}