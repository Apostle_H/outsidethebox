﻿namespace Effect.Data
{
    public interface IEffect
    {
        bool IsAppliable { get; }
        void Apply();
    }

    public interface IEffect<T>
    {
        bool IsAppliable(T value);
        void Apply(T value);
    }
}