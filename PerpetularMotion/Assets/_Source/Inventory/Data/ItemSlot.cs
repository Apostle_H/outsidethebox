﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Inventory.Data
{
    public class ItemSlot
    {
        private List<Func<IItem, bool>> _filters = new();
        
        public IItem StoredItem { get; private set; }

        public event Action<IItem> OnSetItem; 

        public ItemSlot(IItem storedItem) : this() => SetStoredItem(storedItem);
        public ItemSlot() { }

        public bool SetStoredItem(IItem item)
        {
            if (item != default && !Fits(item))
                return false;
            
            if (StoredItem != default)
                StoredItem.OnModify -= CheckAmount;
            StoredItem = item;
            if (StoredItem != default)
                StoredItem.OnModify += CheckAmount;
            
            OnSetItem?.Invoke(item);
            return true;
        }

        public bool Fits(IItem item) => _filters.All(filter => filter(item));

        public void AddFilter(Func<IItem, bool> filter)
        {
            if (filter == default)
                return;
            
            _filters.Add(filter);
        }

        public void RemoveFilter(Func<IItem, bool> filter)
        {
            if (filter == default)
                return;
            
            _filters.Remove(filter);
        }

        private void CheckAmount(IItem _)
        {
            if (StoredItem.Amount > 0)
                return;
            
            SetStoredItem(default);
        }
    }
}