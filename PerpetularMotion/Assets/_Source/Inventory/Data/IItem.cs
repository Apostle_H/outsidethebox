﻿using System;
using Identity;
using UnityEngine;

namespace Inventory.Data
{
    public interface IItem : IIdentity
    {
        string Title { get; }
        string Description { get; }
        Sprite Icon { get; }
        int Amount { get; }

        event Action<IItem> OnModify;

        void Modify(int delta);

        IItem Take(int amount);
    }
}