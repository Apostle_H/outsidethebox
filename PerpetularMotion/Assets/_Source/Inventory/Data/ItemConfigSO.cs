﻿using Identity;
using UnityEngine;

namespace Inventory.Data
{
    public abstract class ItemConfigSO : ScriptableObject, IIdentity
    {
        [field: SerializeField] public string Title { get; private set; }
        [field: SerializeField] public string Description { get; private set; }
        [field: SerializeField] public Sprite Icon { get; private set; }
        public int Id => GetInstanceID();
    }
}