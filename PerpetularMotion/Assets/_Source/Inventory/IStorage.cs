﻿using System;
using System.Collections.Generic;
using Inventory.Data;

namespace Inventory
{
    public interface IStorage
    {
        event Action<ItemSlot> OnItemSlotAdded; 

        bool ContainsItem(int id);
        int CountType(int id);
        void AddItem(IItem item);
        ItemSlot AddEmptySlot();
        void ModifyItem(int id, int delta);
        void Clear();
        IEnumerable<IItem> ToItems();
    }
}