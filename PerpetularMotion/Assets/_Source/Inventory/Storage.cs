﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Inventory.Data;

namespace Inventory
{
    public class Storage : IStorage
    {
        /// <summary>
        /// -id, items-
        /// </summary>
        /// <returns></returns>
        private List<ItemSlot> _itemsSlots = new();
        
        public event Action<ItemSlot> OnItemSlotAdded;

        public bool ContainsItem(int id) => 
            _itemsSlots.FirstOrDefault(itemSlot => itemSlot.StoredItem?.Id == id) != default;

        public int CountType(int id) => 
            _itemsSlots.Where(itemSlot => itemSlot.StoredItem?.Id == id).Sum(itemSlot => itemSlot.StoredItem.Amount);
        
        public void AddItem(IItem item)
        {
            var emptySlot = _itemsSlots.FirstOrDefault(itemSlot => itemSlot.StoredItem == default);

            if (emptySlot != default)
            {
                emptySlot.SetStoredItem(item);
                return;
            }
            
            emptySlot = new ItemSlot();
            emptySlot.SetStoredItem(item);
            _itemsSlots.Add(emptySlot);

            OnItemSlotAdded?.Invoke(emptySlot);
        }

        public ItemSlot AddEmptySlot()
        {
            var emptySlot = new ItemSlot();
            _itemsSlots.Add(emptySlot);

            OnItemSlotAdded?.Invoke(emptySlot);
            return emptySlot;
        }

        public void ModifyItem(int id, int delta)
        {
            if (!ContainsItem(id))
                return;

            foreach (var itemSlot in _itemsSlots.Where(itemSlot => itemSlot.StoredItem?.Id == id))
            {
                var amount = itemSlot.StoredItem.Amount;
                itemSlot.StoredItem.Modify(delta);
                
                if (amount + delta >= 0)
                    break;

                delta += amount;
            }
        }

        public void Clear() => _itemsSlots.Clear();

        public IEnumerable<IItem> ToItems()
        {
            var items = new Dictionary<int, IItem>();
            foreach (var itemSlot in _itemsSlots.Where(itemSlot => itemSlot.StoredItem != default))
            {
                items.TryAdd(itemSlot.StoredItem.Id, itemSlot.StoredItem.Take(0));
                items[itemSlot.StoredItem.Id].Modify(itemSlot.StoredItem.Amount);
            }

            return items.Select(kvp => kvp.Value);
        }
    }
}