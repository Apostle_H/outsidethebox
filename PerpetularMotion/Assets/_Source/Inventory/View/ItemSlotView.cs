﻿using System;
using Inventory.Data;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;

namespace Inventory.View
{
    public class ItemSlotView : VisualElement
    {
        public IItem StoredItem => TargetSlot.StoredItem;

        public ItemSlot TargetSlot { get; private set; }

        private ItemView _itemView;

        public event Action<IItem> OnSetItem;

        public ItemSlotView(ItemSlot itemSlot) : this() => SetTargetSlot(itemSlot);

        public ItemSlotView()
        {
            name = "ItemSlot";
            AddToClassList("item-slot");
            
            _itemView = new ItemView();
            _itemView.Hide();
            Add(_itemView);
            
            _itemView.SetSelfSlot(this);
        }

        public void SetTargetSlot(ItemSlot itemSlot)
        {
            if (TargetSlot != default)
                TargetSlot.OnSetItem -= SetItem;
            TargetSlot = itemSlot;
            if (TargetSlot == default) 
                return;
            
            TargetSlot.OnSetItem += SetItem;
            SetItem(itemSlot.StoredItem);
        }

        private void SetItem(IItem item)
        {
            _itemView.SetTargetItem(item);
            
            OnSetItem?.Invoke(item);
        }
    }
}