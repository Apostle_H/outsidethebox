﻿using UnityEngine.UIElements;
using VContainer;

namespace Inventory.View
{
    public class ItemInfoView : VisualElement
    {
        public Label ItemNameLabel { get; private set; }
        public Label ItemDescriptionLabel { get; private set; }
        
        public ItemInfoView()
        {
            name = "ItemInfoPanel";
            AddToClassList("item-info-panel");
            AddToClassList("text");
            
            pickingMode = PickingMode.Ignore;
            
            ItemNameLabel = new Label() { name = "ItemName" };
            ItemNameLabel.pickingMode = PickingMode.Ignore;
            Add(ItemNameLabel);
            
            ItemDescriptionLabel = new Label() { name = "ItemDescription"};
            ItemDescriptionLabel.pickingMode = PickingMode.Ignore;
            Add(ItemDescriptionLabel);
        }

        public void UpdateInfo(string name, string description)
        {
            ItemNameLabel.text = name;
            ItemDescriptionLabel.text = description;
        }
    }
}