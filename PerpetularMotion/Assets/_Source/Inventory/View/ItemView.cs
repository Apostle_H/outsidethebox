﻿using System;
using DragNDropVE;
using Inventory.Data;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;

namespace Inventory.View
{
    public class ItemView : Image, IDraggableNDroppable
    {
        public IItem TargetItem { get; private set; }

        public VisualElement Parent => parent;
        public VisualElement TargetVE => this;
        public Vector2 Offset => -layout.size / 2;
        public bool HasDropAreaType => true;
        public Type DropAreaType => typeof(ItemSlotView);
        
        public Image Icon => this;
        public Label Amount { get; private set; }

        public ItemSlotView SelfSlot { get; private set; }

        public bool IsBeingDragged { get; private set; }

        public event Action<VisualElement> OnPickedUp;
        public event Action<VisualElement> OnDropped;

        public ItemView(IItem item) : this() => SetTargetItem(item);

        public ItemView()
        {
            AddToClassList("item");
            name = "Item";
            
            Amount = new Label("0") { name = "Amount" };
            Amount.AddToClassList("title");
            Add(Amount);
            
            Amount.pickingMode = PickingMode.Ignore;
        }

        public void SetTargetItem(IItem item)
        {
            if (TargetItem != default)
                TargetItem.OnModify -= UpdateAmount;
            TargetItem = item;
            
            if (TargetItem == null)
            {
                sprite = default;
                Amount.text = "0";
                this.Hide();
            }
            else
            {
                TargetItem.OnModify += UpdateAmount;
                
                sprite = item.Icon;
                Amount.text = item.Amount.ToString();
                this.Show();
            }
        }

        public void SetSelfSlot(ItemSlotView slotView) => SelfSlot = slotView;

        private void UpdateAmount(IItem _)
        {
            if (TargetItem == default)
                return;
            
            Amount.text = TargetItem.Amount.ToString();
        }

        public IDraggableNDroppable PickUp()
        {
            if (SelfSlot == default)
                return this;

            var dragView = new ItemView(TargetItem.Take(1)) { name = "DragItem"};
            dragView.SetSelfSlot(SelfSlot);
            dragView.RegisterCallback<MouseDownEvent>(dragView.MoreItems);
            dragView.IsBeingDragged = true;
            
            return dragView;
        }

        public void Drop(VisualElement dropArea)
        {
            var targetSlot = SelfSlot;
            if (dropArea is ItemSlotView newSlot && 
                ((newSlot.StoredItem == default && newSlot.TargetSlot.Fits(TargetItem)) || 
                 newSlot.StoredItem.Id == TargetItem.Id))
                targetSlot = newSlot;
            
            if (targetSlot.StoredItem == default)
                targetSlot.TargetSlot.SetStoredItem(TargetItem);
            else
                targetSlot.StoredItem.Modify(TargetItem.Amount);

            UnregisterCallback<MouseDownEvent>(MoreItems);
            parent.Remove(this);
            IsBeingDragged = false;
        }

        private void MoreItems(MouseDownEvent evt)
        {
            if (SelfSlot.StoredItem == default || evt.button != 1)
                return;
            
            TargetItem.Modify(1);
            SelfSlot.StoredItem.Modify(-1);
        }
    }
}