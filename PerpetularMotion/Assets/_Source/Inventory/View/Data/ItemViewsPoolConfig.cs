﻿using System;
using Utils.Extensions;
using Utils.Pooling.Data;
using VContainer;

namespace Inventory.View.Data
{
    public class ItemViewsPoolConfig : IPoolConfig<ItemSlotView>
    {
        public Func<ItemSlotView> Factory { get; private set; }
        public Action<ItemSlotView> GetCallback { get; private set; }
        public Action<ItemSlotView> PutCallback { get; private set; }

        [Inject]
        public ItemViewsPoolConfig()
        {
            Factory = () => new ItemSlotView();
            GetCallback = item => item.Show();
            PutCallback = item => item.Hide();
        }
    }
}