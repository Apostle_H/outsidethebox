﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inventory.Data;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UIElements;
using Utils.Extensions;
using Utils.Pooling;
using Utils.Services;
using VContainer;
using VContainer.Unity;

namespace Inventory.View
{
    public class InventoryView : VisualElement, IStartable, IDisposable
    {
        private UIDocument _canvas;
        private InputSystemUIInputModule _input;
        private UIService _uiService;
        
        private IStorage _storage;
        
        private List<ItemSlotView> _itemsSlots = new();

        private DefaultPool<ItemSlotView> _freeItemSlotsViews;

        public VisualElement Toggle { get; private set; }
        public VisualElement InventoryPanel { get; private set; }
        public VisualElement ItemsContainer { get; private set; }
        public ItemInfoView ItemInfoView { get; private set; }

        [Inject]
        public InventoryView(UIDocument canvas, InputSystemUIInputModule input, UIService uiService, IStorage storage, 
            DefaultPool<ItemSlotView> itemSlotsViewsPool)
        {
            _canvas = canvas;
            _input = input;
            _uiService = uiService;
            
            _storage = storage;
            
            _freeItemSlotsViews = itemSlotsViewsPool;
            
            name = "Inventory";
            AddToClassList("inventory");
            pickingMode = PickingMode.Ignore;
            
            InventoryPanel = new VisualElement() { name = "InventoryPanel" };
            InventoryPanel.AddToClassList("inventory-panel");
            Add(InventoryPanel);

            ItemsContainer = new VisualElement() { name = "ItemsContainer" };
            ItemsContainer.AddToClassList("items-container");
            InventoryPanel.Add(ItemsContainer);

            Toggle = new VisualElement() { name = "InventoryToggle" };
            Toggle.AddToClassList("toggle");
            Add(Toggle);

            ItemInfoView = new ItemInfoView();
        }

        public void Start()
        {
            _canvas.rootVisualElement.Add(this);
            InventoryPanel.Hide();
            _canvas.rootVisualElement.Add(ItemInfoView);
            ItemInfoView.Hide();
            
            Bind();
        }

        public void Dispose()
        {
            foreach (var itemSlot in _itemsSlots)
            {
                ExposeItemSlot(itemSlot);
                _freeItemSlotsViews.Put(itemSlot);
            }
            
            ItemInfoView.Hide();
            Expose();
            ExposeItemInfo();
        }

        private void Bind()
        {
            Toggle.RegisterCallback<MouseDownEvent>(ToggleInventoryBtn);
            
            _storage.OnItemSlotAdded += AddItemSlot;
        }

        private void Expose()
        {
            Toggle.UnregisterCallback<MouseDownEvent>(ToggleInventoryBtn);
            
            _storage.OnItemSlotAdded -= AddItemSlot;
        }

        private void BindItemInfo()
        {
            _input.point.action.performed += CheckForInfo;
        }

        private void ExposeItemInfo() => _input.point.action.performed -= CheckForInfo;

        private void BindItemSlot(ItemSlotView itemSlot) => itemSlot.OnSetItem += CheckSlotsAvailability;

        private void ExposeItemSlot(ItemSlotView itemSlot) => itemSlot.OnSetItem -= CheckSlotsAvailability;

        private void ToggleInventoryBtn(MouseDownEvent evt) => ToggleInventory();

        private void ToggleInventory()
        {
            if (InventoryPanel.IsVisible())
            {
                InventoryPanel.Hide();
                ExposeItemInfo();
            }
            else
            {
                InventoryPanel.Show();
                BindItemInfo();
            }
        }

        private void AddItemSlot(ItemSlot itemSlot)
        {
            var emptySlot = AddItemSlot();
            BindItemSlot(emptySlot);
            emptySlot.SetTargetSlot(itemSlot);
        }

        private ItemSlotView AddItemSlot()
        {
            var itemSlot = _freeItemSlotsViews.Get();
            
            _itemsSlots.Add(itemSlot);
            ItemsContainer.Add(itemSlot);

            return itemSlot;
        }

        private void CheckSlotsAvailability(IItem item)
        {
            if (item == default || _itemsSlots.Any(itemSlotView => itemSlotView.StoredItem == default))
                return;
            
            _storage.AddEmptySlot();
        }
        
        private void CheckForInfo(InputAction.CallbackContext ctx)
        {
            var target = _uiService.GetMouseOverUIElement();
            if (target is not ItemView itemView || itemView.TargetItem == default || itemView.IsBeingDragged)
            {
                if (ItemInfoView.IsVisible())
                    ItemInfoView.Hide();
                return;
            }

            if (!ItemInfoView.IsVisible())
                ItemInfoView.Show();

            var offset = new Vector2(0, itemView.layout.size.y);
            ItemInfoView.UpdateInfo(itemView.TargetItem.Title, itemView.TargetItem.Description);
            ItemInfoView.SetTranslate(itemView.LocalToWorld(itemView.transform.position) + offset);
        }
    }
}