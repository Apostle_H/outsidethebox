﻿using System;
using System.Linq;
using InputSystem.Garden;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UIElements;
using Utils.Extensions;
using Utils.Services;
using VContainer;
using VContainer.Unity;
using View;

namespace DragNDropVE
{
    public class DraggerNDropper : IStartable, IDisposable
    {
        private InputSystemUIInputModule _actions;
        private UIService _uiService;
        private TopView _topView;

        private IDraggableNDroppable _target;
        
        private VisualElement _beforeDragParent;
        private int _beforeDragIndexInParent;

        [Inject]
        public DraggerNDropper(InputSystemUIInputModule actions, UIService uiService, TopView topView)
        {
            _actions = actions;
            _uiService = uiService;
            _topView = topView;
        }
        
        public void Start() => Bind();

        public void Dispose() => Expose();

        private void Bind()
        {
            _actions.leftClick.action.performed += TryDrag;
            _actions.leftClick.action.performed += TryDrop;
        }

        private void Expose()
        {
            TryDrop(default);
            
            _actions.leftClick.action.performed -= TryDrag;
            _actions.leftClick.action.performed -= TryDrop;
        }

        private void TryDrag(InputAction.CallbackContext ctx)
        {
            if (ctx.ReadValue<float>() < 0.5f)
                return;
            
            var target = _uiService.GetMouseOverUIElement();
            if (target == default || target is not IDraggableNDroppable draggableNDroppable)
                return;
            
            _target = draggableNDroppable;
            
            _beforeDragParent = _target.Parent;
            _beforeDragIndexInParent = _beforeDragParent.IndexOf(_target.TargetVE);
            
            _topView.CopyResolvedSizeStyle(_beforeDragParent);
            _target = _target.PickUp();
            _topView.Add(_target.TargetVE);

            _target.TargetVE.SetTranslateFromScreenPoint(_actions.point.action.ReadValue<Vector2>(), _target.Offset);
            _actions.point.action.performed += MoveTarget;
        }
        
        private void MoveTarget(InputAction.CallbackContext ctx)
        {
            var inputMousePos = ctx.ReadValue<Vector2>();
            _target.TargetVE.SetTranslateFromScreenPoint(inputMousePos, _target.Offset);
        }

        private void TryDrop(InputAction.CallbackContext ctx)
        {
            if (ctx.ReadValue<float>() > 0.5f)
                return;
            
            if (_target == default)
                return;


            VisualElement dropArea = default;
            if (_target.HasDropAreaType)
            {
                var mouseOverUIElements = _uiService.GetMouseOverUIElements();
                foreach (var mouseOverUIElement in mouseOverUIElements)
                {
                    if (mouseOverUIElement.GetType() != _target.DropAreaType) 
                        continue;
                    
                    dropArea = mouseOverUIElement;
                    break;
                }
            }
            
            if (dropArea != default)
                dropArea.Add(_target.TargetVE);
            else
                _beforeDragParent.Insert(_beforeDragIndexInParent, _target.TargetVE);
            
            _target.Drop(dropArea);
            
            _target = default;
            
            _actions.point.action.performed -= MoveTarget;
        }
    }
}