﻿using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace DragNDropVE
{
    public interface IDraggableNDroppable
    {
        VisualElement Parent { get; }
        VisualElement TargetVE { get; }
        Vector2 Offset { get; }
        
        bool HasDropAreaType { get; }
        Type DropAreaType { get; }
        
        event Action<VisualElement> OnPickedUp;
        event Action<VisualElement> OnDropped;
        
        IDraggableNDroppable PickUp();
        void Drop(VisualElement dropArea);
    }
}