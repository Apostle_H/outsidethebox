﻿using System;
using UnityEngine;

namespace Utils.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class WorldVectorAttribute : PropertyAttribute
    {
        
    }
}