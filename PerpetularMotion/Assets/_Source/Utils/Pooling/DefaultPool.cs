﻿using System.Collections.Generic;
using Utils.Pooling.Data;
using VContainer;

namespace Utils.Pooling
{
    public class DefaultPool<T> : IPool<T>
    {
        private IPoolConfig<T> _config;

        private Queue<T> _pool = new();

        [Inject]
        public DefaultPool(IPoolConfig<T> config) => _config = config;

        public T Get()
        {
            if (_pool.Count < 1)
                Add();

            var item = _pool.Dequeue();
            _config.GetCallback?.Invoke(item);
            return item;
        }

        public void Put(T item)
        {
            _config.PutCallback?.Invoke(item);
            _pool.Enqueue(item);
        }

        private void Add() => Put(_config.Factory());
    }
}