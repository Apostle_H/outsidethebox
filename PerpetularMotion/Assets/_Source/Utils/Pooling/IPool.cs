﻿namespace Utils.Pooling
{
    public interface IPool<T>
    {
        public T Get();
        public void Put(T item);
    }
}