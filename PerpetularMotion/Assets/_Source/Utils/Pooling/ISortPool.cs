﻿namespace Utils.Pooling
{
    public interface ISortPool<T, TSort>
    {
        public T Get(TSort sortValue);
        public void Put(T item, TSort sortValue);
    }
}