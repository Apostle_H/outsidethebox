﻿using System.Collections.Generic;
using Utils.Pooling.Data;
using VContainer;

namespace Utils.Pooling
{
    public class DefaultSortPool<T, TSort> : ISortPool<T, TSort>
    {
        private ISortPoolConfig<T, TSort> _config;

        private Dictionary<TSort, Queue<T>> _pool = new();

        [Inject]
        public DefaultSortPool(ISortPoolConfig<T, TSort> config) => _config = config;

        public T Get(TSort sortValue)
        {
            if (!_pool.ContainsKey(sortValue))
                _pool.Add(sortValue, new Queue<T>());

            if (_pool[sortValue].Count < 1)
                Add(sortValue);

            var item = _pool[sortValue].Dequeue();
            _config.GetCallback?.Invoke(item);
            return item;
        }

        public void Put(T item, TSort sortValue)
        {
            _config.PutCallback?.Invoke(item);
            _pool[sortValue].Enqueue(item);
        }

        private void Add(TSort sortValue) => Put(_config.Factory(sortValue), sortValue);
    }
}