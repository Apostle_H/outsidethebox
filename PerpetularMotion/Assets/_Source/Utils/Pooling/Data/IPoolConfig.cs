﻿using System;

namespace Utils.Pooling.Data
{
    public interface IPoolConfig<T>
    {
        Func<T> Factory { get; }
        Action<T> GetCallback { get; }
        Action<T> PutCallback { get; }
    }
}