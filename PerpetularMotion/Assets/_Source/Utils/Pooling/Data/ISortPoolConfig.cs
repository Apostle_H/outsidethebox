﻿using System;

namespace Utils.Pooling.Data
{
    public interface ISortPoolConfig<T, TSort>
    {
        Func<TSort, T> Factory { get; }
        Action<T> GetCallback { get; }
        Action<T> PutCallback { get; }
    }
}