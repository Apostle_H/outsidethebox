﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utils.Extensions
{
    public static class IEnumerableExtensions
    {
        public static bool CompareSorted<T1, T2>(this IEnumerable<T1> enumerable1, IEnumerable<T2> enumerable2, Func<T1, T2, bool> comparer)
        {
            var array1 = enumerable1.ToArray();
            var array2 = enumerable2.ToArray();
            if (array1.Length != array2.Length)
                return false;

            return !array1.Where((item, i) => !comparer.Invoke(item, array2[i])).Any();
        }

        public static bool CompareSorted<T1, T2>(this IEnumerable<T1> enumerable1, IEnumerable<T2> enumerable2, Func<T2, T1, bool> comparer) => 
            enumerable2.CompareSorted(enumerable1, comparer);
    }
}