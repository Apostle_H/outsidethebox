﻿using System;

namespace Utils.Extensions
{
    public static class EnumExtensions
    {
        public static string Shorten(this Enum value, int charCount = 1)
        {
            var stringValue = value.ToString();

            var shortenString = String.Empty;
            for (var i = 0; i < charCount; i++)
            {
                if (i >= stringValue.Length)
                    break;
                
                shortenString += stringValue[i];
            }

            if (charCount < value.ToString().Length)
                shortenString += '.';
            return shortenString;
        }
    }
}