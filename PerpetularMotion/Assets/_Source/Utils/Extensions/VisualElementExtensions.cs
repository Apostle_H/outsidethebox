﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Utils.Extensions
{
    public static class VisualElementExtensions
    {
        public static void Show(this VisualElement visualElement) => 
            visualElement.style.display = DisplayStyle.Flex;

        public static void Hide(this VisualElement visualElement) => 
            visualElement.style.display = DisplayStyle.None;

        public static void Toggle(this VisualElement visualElement, bool onOff)
        {
            if (onOff)
                visualElement.Show();
            else
                visualElement.Hide();
        }

        public static bool IsVisible(this VisualElement visualElement) =>
            visualElement.style.display == DisplayStyle.Flex;

        public static void SetTranslateRotation(this VisualElement visualElement, Vector2 position, float rotation, 
            LengthUnit lengthUnit = LengthUnit.Pixel)
        {
            visualElement.SetTranslate(position, lengthUnit);
            visualElement.SetRotation(rotation);
        }
        
        public static void SetTranslate(this VisualElement visualElement, Vector2 position, 
            LengthUnit lengthUnit = LengthUnit.Pixel)
        {
            var xTranslate = new Length(position.x, lengthUnit);
            var yTranslate = new Length(position.y, lengthUnit);
            visualElement.style.translate = new Translate(xTranslate, yTranslate);
        }

        public static void SetRotation(this VisualElement visualElement, float rotation)
        {
            var styleRotation = new Angle(rotation, AngleUnit.Degree);
            visualElement.style.rotate = new Rotate(styleRotation);
        }

        public static void SetTranslateFromScreenPoint(this VisualElement visualElement, Vector2 screenPos,
            Vector2 offset)
        {
            var inputMousePosScreen = new Vector2(screenPos.x / Screen.width, screenPos.y / Screen.height);
            var flippedPosition = new Vector2(inputMousePosScreen.x, 1 - inputMousePosScreen.y);
            var panelPosition = flippedPosition * visualElement.panel.visualTree.layout.size;
            var localPosition = visualElement.parent.WorldToLocal(panelPosition) + offset;
            visualElement.SetTranslate(localPosition);
        }

        public static void CopyResolvedSizeStyle(this VisualElement visualElement, VisualElement copyFrom)
        {
            visualElement.style.height = copyFrom.resolvedStyle.height;
            visualElement.style.width = copyFrom.resolvedStyle.width;
            
            visualElement.style.paddingTop = copyFrom.resolvedStyle.paddingTop;
            visualElement.style.paddingBottom = copyFrom.resolvedStyle.paddingBottom;
            visualElement.style.paddingLeft = copyFrom.resolvedStyle.paddingLeft;
            visualElement.style.paddingRight = copyFrom.resolvedStyle.paddingRight;
            
            visualElement.style.borderTopWidth = copyFrom.resolvedStyle.borderTopWidth;
            visualElement.style.borderBottomWidth = copyFrom.resolvedStyle.borderBottomWidth;
            visualElement.style.borderLeftWidth = copyFrom.resolvedStyle.borderLeftWidth;
            visualElement.style.borderRightWidth = copyFrom.resolvedStyle.borderRightWidth;

            visualElement.style.borderTopLeftRadius = copyFrom.resolvedStyle.borderTopLeftRadius;
            visualElement.style.borderTopRightRadius = copyFrom.resolvedStyle.borderTopRightRadius;
            visualElement.style.borderBottomLeftRadius = copyFrom.resolvedStyle.borderBottomLeftRadius;
            visualElement.style.borderBottomRightRadius = copyFrom.resolvedStyle.borderBottomRightRadius;

            visualElement.style.marginTop = copyFrom.resolvedStyle.marginTop;
            visualElement.style.marginBottom = copyFrom.resolvedStyle.marginBottom;
            visualElement.style.marginLeft = copyFrom.resolvedStyle.marginLeft;
            visualElement.style.marginRight = copyFrom.resolvedStyle.marginRight;

            visualElement.style.scale = copyFrom.resolvedStyle.scale;
        }
    }
}