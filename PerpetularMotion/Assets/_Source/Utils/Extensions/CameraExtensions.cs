﻿using UnityEngine;

namespace Utils.Extensions
{
    public static class CameraExtensions
    {
        public static Vector3 ScreenPositionToPositionOnPlane(this Camera camera, Vector2 screenPosition, float planeY)
        {
            var ray = camera.ScreenPointToRay(screenPosition);
            var delta = ray.origin.y - planeY;
            var dirNorm = ray.direction / ray.direction.y;
            
            return ray.origin - dirNorm * delta;
        }
    }
}