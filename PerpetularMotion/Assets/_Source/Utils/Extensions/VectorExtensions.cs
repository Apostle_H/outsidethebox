﻿using UnityEngine;

namespace Utils.Extensions
{
    public static class VectorExtensions
    {
        public static Vector3 ToXZ(this Vector2 target) => new Vector3(target.x, 0f, target.y);
        
        public static Vector2 ToXZ(this Vector3 target) => new Vector2(target.x, target.z);

        public static Vector3 ReplaceXYZ(this Vector3 target, Vector3 replace, 
            bool replaceX = false, bool replaceY = false, bool replaceZ = false)
        {
            var result = target;
            if (replaceX)
                result.x = replace.x;
            if (replaceY)
                result.y = replace.y;
            if (replaceZ)
                result.z = replace.z;

            return result;
        }

        public static Vector3 ReplaceX(this Vector3 target, Vector3 replace) => target.ReplaceXYZ(replace, replaceX: true);
        
        public static Vector3 ReplaceY(this Vector3 target, Vector3 replace) => target.ReplaceXYZ(replace, replaceY: true);

        public static Vector3 ReplaceZ(this Vector3 target, Vector3 replace) => target.ReplaceXYZ(replace, replaceZ: true);
        
        public static Vector3 ReplaceXY(this Vector3 target, Vector3 replace) => 
            target.ReplaceXYZ(replace, replaceX: true, replaceY: true);
        
        public static Vector3 ReplaceXZ(this Vector3 target, Vector3 replace) => 
            target.ReplaceXYZ(replace, replaceX: true, replaceZ: true);
        
        public static Vector3 ReplaceYZ(this Vector3 target, Vector3 replace) => 
            target.ReplaceXYZ(replace, replaceY: true, replaceZ: true);
        
        public static Vector2Int SnapVector(this Vector2 position, Vector2 snapping)
        {
            var x = (int)(position.x / snapping.x) / 2;
            var y = (int)(position.y / snapping.y) / 2;
            
            return new Vector2Int(x, y);
        }
    }
}