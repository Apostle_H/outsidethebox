﻿using Garden.Deck.Card.Data;
using UnityEngine;

namespace Garden.Deck.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/ConfigSO", fileName = "NewGardenDeckConfigSO")]
    public class GardenDeckConfigSO : ScriptableObject
    {
        [field: SerializeField] public GardenCardConfigSO[] CardConfigsSO { get; private set; }
    }
}