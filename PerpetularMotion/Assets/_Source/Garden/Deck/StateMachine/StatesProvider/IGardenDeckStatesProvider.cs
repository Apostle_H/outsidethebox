﻿using StateMachine;

namespace Garden.Deck.StateMachine.StatesProvider
{
    public interface IGardenDeckStatesProvider : IStatesProvider { }
}