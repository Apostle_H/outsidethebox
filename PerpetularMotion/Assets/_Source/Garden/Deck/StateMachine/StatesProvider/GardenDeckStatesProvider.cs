﻿using System;
using System.Collections.Generic;
using System.Linq;
using Garden.Deck.StateMachine.States;
using StateMachine.States;
using VContainer;

namespace Garden.Deck.StateMachine.StatesProvider
{
    public class GardenDeckStatesProvider : IGardenDeckStatesProvider
    {
        public IState StartingState { get; }
        
        private readonly Dictionary<Type, IState> _states = new();

        [Inject]
        public GardenDeckStatesProvider(WaitingState waiting, GardenDeckInitializeState initialize,
            GardenDeckChooseCardState chooseCard, GardenDeckNextRoundState nextRound)
        {
            StartingState = waiting;
            
            _states.Add(waiting.GetType(), waiting);
            _states.Add(initialize.GetType(), initialize);
            _states.Add(chooseCard.GetType(), chooseCard);
            _states.Add(nextRound.GetType(), nextRound);
        }
        
        public Dictionary<Type, IState> GetStates() => 
            _states.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
    }
}