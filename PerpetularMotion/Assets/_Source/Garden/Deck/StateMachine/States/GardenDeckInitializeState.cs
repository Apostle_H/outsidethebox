﻿using StateMachine;
using StateMachine.States;
using VContainer;

namespace Garden.Deck.StateMachine.States
{
    public class GardenDeckInitializeState : IState
    {
        private GardenDeckBuilder _deckBuilder;
        
        public IStateMachine Owner { get; set; }

        [Inject]
        public GardenDeckInitializeState(GardenDeckBuilder deckBuilder) => _deckBuilder = deckBuilder;

        public void Enter()
        {
            _deckBuilder.InitDeck();
            ToChooseCardState();
        }

        public void Update() { }

        public void Exit() { }
        
        private void ToChooseCardState() => Owner.Switch<GardenDeckChooseCardState>();
    }
}