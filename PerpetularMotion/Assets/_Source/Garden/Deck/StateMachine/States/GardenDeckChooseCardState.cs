﻿using StateMachine;
using StateMachine.States;

namespace Garden.Deck.StateMachine.States
{
    public class GardenDeckChooseCardState : IState
    {
        public IStateMachine Owner { get; set; }
        
        public void Enter() { }

        public void Update() { }

        public void Exit() { }
    }
}