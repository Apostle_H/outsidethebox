﻿using Deck.Hand;
using Garden.Deck.Card;
using StateMachine;
using StateMachine.States;
using UnityEngine;
using VContainer;

namespace Garden.Deck.StateMachine.States
{
    public class GardenDeckNextRoundState : IState
    {
        private IHand<IGardenCard, Vector2Int> _hand;
        
        public IStateMachine Owner { get; set; }
        
        [Inject]
        public GardenDeckNextRoundState(IHand<IGardenCard, Vector2Int> hand) => _hand = hand;

        public void Enter()
        {
            for (var i = _hand.CardsCount; i < _hand.DefaultHandSize; i++)
                _hand.Draw();
            ToChooseCardState();
        }

        public void Update() { }

        public void Exit() { }
        
        private void ToChooseCardState() => Owner.Switch<GardenDeckChooseCardState>();
    }
}