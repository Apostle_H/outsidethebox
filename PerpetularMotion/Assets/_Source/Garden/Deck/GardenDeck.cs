﻿using System.Collections.Generic;
using System.Linq;
using Deck;
using Garden.Deck.Card;
using UnityEngine;

namespace Garden.Deck
{
    public class GardenDeck : IDeck<IGardenCard>
    {
        private readonly List<IGardenCard> _drawPile = new();
        private readonly List<IGardenCard> _discardPile = new();
        private readonly List<IGardenCard> _lostPile = new();

        public List<IGardenCard> GetDrawPile() => _drawPile.ToList();

        public List<IGardenCard> GetDiscardPile() => _discardPile.ToList();

        public List<IGardenCard> GetLostPile() => _lostPile.ToList();

        public bool Draw(out IGardenCard card)
        {
            card = default;
            if (_drawPile.Count == 0)
                if (_discardPile.Count == 0)
                    return false;
                else
                    Shuffle();

            var lastIndex = _drawPile.Count - 1;
            card = _drawPile[lastIndex];
            _drawPile.RemoveAt(lastIndex);

            return true;
        }

        public void Discard(IGardenCard card)
        {
            if (_discardPile.Contains(card))
                return;
            
            _discardPile.Add(card);
        }

        public void Lost(IGardenCard card)
        {
            if (_lostPile.Contains(card))
                return;
            
            _lostPile.Add(card);
        }

        public void Shuffle()
        {
            _discardPile.AddRange(_drawPile);
            _drawPile.Clear();
            
            foreach (var card in _discardPile.OrderBy(_ => Random.value))
                _drawPile.Add(card);
            
            _discardPile.Clear();
        }

        public bool Recover(IGardenCard card)
        {
            if (!_lostPile.Contains(card))
                return false;

            _lostPile.Remove(card);
            return true;
        }
    }
}