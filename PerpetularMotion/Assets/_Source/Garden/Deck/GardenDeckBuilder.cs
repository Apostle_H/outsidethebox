﻿using System.Collections.Generic;
using Deck;
using Deck.Card;
using Effect.Data;
using Garden.Deck.Card;
using Garden.Deck.Card.Data;
using Garden.Deck.Data;
using Garden.Field.Effects;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Garden.Deck
{
    public class GardenDeckBuilder
    {
        private readonly IObjectResolver _container;
        private readonly GardenCardConfigSO[] _cards;
        private readonly IDeck<IGardenCard> _deck;
        
        [Inject]
        public GardenDeckBuilder(IObjectResolver container, GardenDeckConfigSO configSO, IDeck<IGardenCard> deck)
        {
            _container = container;
            _cards = configSO.CardConfigsSO;
            _deck = deck;
        }

        public void InitDeck()
        {
            foreach (var cardConfig in _cards) 
                _deck.Discard(Build(cardConfig));
            
            _deck.Shuffle();
        }

        public GardenCard Build(GardenCardConfigSO cardConfigSO)
        {
            var effects = new List<IFieldEffect>();
            foreach (var cardEffectSO in cardConfigSO.CardEffectsSO)
            {
                var newEffect = cardEffectSO.Build(); 
                _container.Inject(newEffect);
                    
                effects.Add(newEffect);
            }

            var card = new GardenCard(cardConfigSO.Title, effects, cardConfigSO.IsLost, cardConfigSO.TargetTilesOffset);
            _container.Inject(card);

            return card;
        }
    }
}