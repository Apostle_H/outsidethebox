﻿using System;
using System.Collections.Generic;
using Deck;
using Deck.Hand;
using Deck.Hand.Data;
using Garden.Deck.Card;
using UnityEngine;
using VContainer;

namespace Garden.Deck
{
    public class GardenHand : IHand<IGardenCard, Vector2Int>
    {
        private readonly IDeck<IGardenCard> _deck;

        private readonly List<IGardenCard> _cards = new();

        public int DefaultHandSize { get; private set; }
        public int CardsCount { get; private set; } = 0;

        public event Action<IGardenCard> OnDraw;
        public event Action<IGardenCard> OnAdd;
        public event Action<IGardenCard> OnPlayed;
        public event Action<IGardenCard> OnDiscard;
        public event Action<IGardenCard> OnLost;

        [Inject]
        public GardenHand(HandConfigSO configSO, IDeck<IGardenCard> deck)
        {
            DefaultHandSize = configSO.Size;
            _deck = deck;
        }

        public void Draw()
        {
            if (!_deck.Draw(out var card))
                return;
            
            Add(card);
            
            OnDraw?.Invoke(card);
        }

        public void Add(IGardenCard card)
        {
            _cards.Add(card);
            CardsCount++;
            
            OnAdd?.Invoke(card);
        }

        public void Play(int cardIndex, Vector2Int primaryTargetTileIndex)
        {
            if (cardIndex < 0 || cardIndex >= _cards.Count)
                return;
            
            var card = _cards[cardIndex];
            if (!card.IsAppliable(primaryTargetTileIndex))
                return;
            
            card.Apply(primaryTargetTileIndex);    
            Played(card);
        }

        public void Recover(IGardenCard card)
        {
            if (!_deck.Recover(card))
                return;
            
            _cards.Add(card);
        }

        private void Played(IGardenCard card)
        {
            OnPlayed?.Invoke(card);
            
            _cards.Remove(card);

            if (!card.IsLost)
                _deck.Discard(card);
            else
                _deck.Lost(card);
            CardsCount--;

            if (!card.IsLost)
                OnDiscard?.Invoke(card);
            else
                OnLost?.Invoke(card);
        }
    }
}