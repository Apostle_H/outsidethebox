﻿using Deck.Card;
using Garden.Deck.Card;
using UnityEngine.UIElements;

namespace Garden.Deck.View
{
    public class CardOverview
    {
        public IGardenCard TargetCard { get; private set; }
        public VisualElement Root { get; private set; }

        private Label _name;
        private VisualElement _lostIcon;

        public CardOverview(VisualElement root)
        {
            Root = root;

            _name = Root.Q<Label>("NameLabel");
            _lostIcon = Root.Q<VisualElement>("LostIcon");
        }

        public void SetTargetCard(IGardenCard card)
        {
            TargetCard = card;
            
            _name.text = card.Title;
            _lostIcon.visible = card.IsLost;
        }

        public void Show() => Root.style.display = DisplayStyle.Flex;
        
        public void Hide() => Root.style.display = DisplayStyle.None;
    }
}