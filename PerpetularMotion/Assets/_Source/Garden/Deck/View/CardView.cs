﻿using System;
using Deck.Card;
using DragNDropVE;
using Garden.Deck.Card;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;

namespace Garden.Deck.View
{
    public class CardView : VisualElement, IDraggableNDroppable
    {
        public IGardenCard TargetCard { get; private set; }
        public int InHandIndex { get; private set; }

        public VisualElement Parent => parent;
        public VisualElement TargetVE => this;
        public Vector2 Offset => -layout.size / 2;
        public bool HasDropAreaType => false;
        public Type DropAreaType => typeof(Type);
        
        public Label NameLabel { get; private set; }
        public Label InfoLabel { get; private set; }
        public VisualElement LostIcon { get; private set; }

        public event Action<CardView> OnHover;
        public event Action<CardView> OnUnhover;
        public event Action<VisualElement> OnPickedUp;
        public event Action<VisualElement> OnDropped;

        private const string USS_HOVER_CLASS = "card-hover";
        private const string USS_LMB_DOWN_CLASS = "card-lmb-down";
        private const string USS_MINIMIZE_CLASS = "card-minimize";

        public CardView(int inHandIndex)
        {
            name = "Card";
            AddToClassList("card");

            NameLabel = new Label() { name = "CardNameLabel" };
            NameLabel.AddToClassList("title");
            Add(NameLabel);
            
            InfoLabel = new Label() { name = "CardInfoLabel" };
            InfoLabel.AddToClassList("text");
            Add(InfoLabel);

            LostIcon = new VisualElement() { name = "CardLostIcon" };
            LostIcon.AddToClassList("lost-icon");
            Add(LostIcon);
            
            InHandIndex = inHandIndex;
        }

        public void Bind()
        {
            RegisterCallback<MouseEnterEvent>(Hover);
            RegisterCallback<MouseLeaveEvent>(Unhover);
        }

        public void Expose()
        {
            UnregisterCallback<MouseEnterEvent>(Hover);
            UnregisterCallback<MouseLeaveEvent>(Unhover);
        }

        public void SetTargetCard(IGardenCard card)
        {
            TargetCard = card;
            
            NameLabel.text = card.Title;
            LostIcon.visible = card.IsLost;
        }
        
        public void Minimize() => AddToClassList(USS_MINIMIZE_CLASS);

        public void Normalize() => RemoveFromClassList(USS_MINIMIZE_CLASS);

        private void Hover(MouseEnterEvent evt)
        {
            AddToClassList(USS_HOVER_CLASS);
            
            OnHover?.Invoke(this);
        }

        private void Unhover(MouseLeaveEvent evt)
        {
            RemoveFromClassList(USS_HOVER_CLASS);
            
            OnUnhover?.Invoke(this);
        }

        public IDraggableNDroppable PickUp()
        {
            this.SetRotation(0);
            Minimize();
            
            AddToClassList(USS_LMB_DOWN_CLASS);
            OnPickedUp?.Invoke(this);
            return this;
        }

        public void Drop(VisualElement newParent)
        {
            Normalize();
            
            this.SetTranslate(Vector2.zero);
            RemoveFromClassList(USS_LMB_DOWN_CLASS);
            OnDropped?.Invoke(this);
        }
    }
}