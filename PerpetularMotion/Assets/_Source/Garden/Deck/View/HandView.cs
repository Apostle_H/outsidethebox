﻿using System;
using System.Collections.Generic;
using System.Linq;
using Deck.Hand;
using Field.Grid.Selector;
using Garden.Deck.Card;
using Garden.Deck.View.Data;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;
using VContainer;
using VContainer.Unity;

namespace Garden.Deck.View
{
    public class HandView : IStartable, IDisposable
    {
        private UIDocument _canvas;
        private VisualTreeAsset _cardTreeAsset;
        private Vector2 _cardOffset;
        private float _maxRotationDeg;

        private IGridTileSelector _gridTileSelector;
            
        private List<CardView> _cardsViews = new();
        private CardOverview _cardOverview;

        private VisualElement _handRoot;
        private VisualElement _cardOverviewRoot;
        
        public IHand<IGardenCard, Vector2Int> Hand { get; private set; }
        public VisualElement Root { get; private set; }

        [Inject]
        public HandView(GardenHandViewConfigSO configSO, UIDocument canvas, IHand<IGardenCard, Vector2Int> hand, 
            IGridTileSelector gridTileSelector)
        {
            _canvas = canvas;
            _cardTreeAsset = configSO.CardTreeAsset;
            _cardOffset = configSO.CardOffset;
            _maxRotationDeg = configSO.MaxRotationDeg;
            
            Hand = hand;
            _gridTileSelector = gridTileSelector;
        }

        public void Start()
        {
            Root = _canvas.rootVisualElement;
            _handRoot = Root.Q<VisualElement>("Hand");
            _cardOverviewRoot = Root.Q<VisualElement>("CardOverview");
            
            for (var i = 0; i < Hand.DefaultHandSize; i++)
                CreateCard(i);

            var cardOverviewRoot = _cardTreeAsset.Instantiate().Q<VisualElement>("Card");
            _cardOverview = new CardOverview(cardOverviewRoot);
            _cardOverviewRoot.Add(_cardOverview.Root);
            _cardOverview.Hide();
            
            Bind();
        }

        public void Dispose() => Expose();

        private void Bind()
        {
            Hand.OnAdd += Add;
            Hand.OnPlayed += Played;
            Hand.OnDiscard += Discard;
            Hand.OnLost += Lost;
        }

        private void Expose()
        {
            Hand.OnAdd -= Add;
            Hand.OnPlayed -= Played;
            Hand.OnDiscard -= Discard;
            Hand.OnLost -= Lost;

            foreach (var cardView in _cardsViews)
                ExposeCardView(cardView);
        }

        private void BindCardView(CardView cardView)
        {
            cardView.Bind();
                
            cardView.OnHover += CardHover;
            cardView.OnUnhover += CardUnhover;
            cardView.OnDropped += CardDeselected;
        }

        private void ExposeCardView(CardView cardView)
        {
            cardView.Expose();
                
            cardView.OnHover -= CardHover;
            cardView.OnUnhover -= CardUnhover;
            cardView.OnDropped -= CardDeselected;
        }

        private void Add(IGardenCard card)
        {
            if (_cardsViews.Count < Hand.CardsCount)
                CreateCard(_cardsViews.Count);

            _cardsViews[Hand.CardsCount - 1].SetTargetCard(card);
            _cardsViews[Hand.CardsCount - 1].Show();

            for (var i = 0; i < Hand.CardsCount; i++)
                PlaceCard(i);
        }
        
        private void Played(IGardenCard card) { }

        private void Discard(IGardenCard card)
        {
            var gapIndex = _cardsViews.First(cardView => cardView.TargetCard == card).InHandIndex;
            ResetCards(gapIndex);
        }

        private void Lost(IGardenCard card)
        {
            var gapIndex = _cardsViews.First(cardView => cardView.TargetCard == card).InHandIndex;
            ResetCards(gapIndex);
        }

        private void ResetCards(int gapIndex)
        {
            for (var i = 0; i < Hand.CardsCount; i++)
            {
                PlaceCard(i);
                if (i >= gapIndex)
                    _cardsViews[i].SetTargetCard(_cardsViews[i + 1].TargetCard);
            }

            _cardsViews[Hand.CardsCount].Hide();
        }
        
        private void CardHover(CardView cardView)
        {
            _cardOverview.SetTargetCard(cardView.TargetCard);
            _cardOverview.Show();   
            
            cardView.BringToFront();
        }

        private void CardUnhover(CardView cardView)
        {
            _cardOverview.Hide();
            
            for (var i = cardView.InHandIndex; i >= 0; i--)
                _cardsViews[i].SendToBack();
        }

        private void CardDeselected(VisualElement cardView)
        {
            var cardViewCasted = cardView as CardView;
            
            PlaceCard(cardViewCasted.InHandIndex);
            Hand.Play(cardViewCasted.InHandIndex, _gridTileSelector.SelectedTileIndex);
        }

        private void PlaceCard(int cardIndex)
        {
            var halfCardsCount = Hand.CardsCount / 2;
            var isEvenCount = Hand.CardsCount % 2 == 0;
            var cardTransform = CountCardTransform(halfCardsCount, isEvenCount, cardIndex);
            _cardsViews[cardIndex].SetTranslateRotation(cardTransform, cardTransform.z, LengthUnit.Percent);
        }

        private Vector3 CountCardTransform(int halfCardsCount, bool isEvenCount, int cardIndex)
        {
            var multiplier = (float)(cardIndex - halfCardsCount);
            if (isEvenCount)
                multiplier += 0.5f;
            
            var xOffset = _cardOffset.x * multiplier;
            var yOffset = _cardOffset.y * Mathf.Ceil(Mathf.Abs(multiplier));
            var rotation = _maxRotationDeg / Hand.CardsCount * multiplier;
            return new Vector3(xOffset, yOffset, rotation);
        }

        private void CreateCard(int index)
        {
            var card = new CardView(index);
            _cardsViews.Add(card);

            _handRoot.Add(card);
            card.Hide();
            
            BindCardView(card);
        }
    }
}