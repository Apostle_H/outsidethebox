﻿using System.Collections.Generic;
using System.Linq;
using Deck.Card;
using Garden.Deck.Card;
using Garden.Deck.View.Data;
using UnityEngine;
using UnityEngine.UIElements;

namespace Garden.Deck.View
{
    public class CardPileView
    {
        private int _cardsPerRow;
        private Vector2 _spacing;
        private float _topOffset;
        private float _maxCardHeight;
        
        private VisualTreeAsset _cardTree;

        private List<CardOverview> _cardOverviews = new();
        
        public VisualElement Root { get; private set; }
        public VisualElement CardsBoard { get; private set; }
        public bool Visible { get; private set; }

        public CardPileView(int cardsPerRow, Vector2 spacing, float topOffset, float maxCardHeight, VisualElement root,
            VisualTreeAsset cardTree)
        {
            _cardsPerRow = cardsPerRow;
            _spacing = spacing;
            _topOffset = topOffset;
            _maxCardHeight = maxCardHeight;
            
            Root = root;
            CardsBoard = Root.Q<VisualElement>("CardsBoard");
            
            _cardTree = cardTree;
        }

        public void Show(IEnumerable<IGardenCard> cards)
        {
            var cardsList = cards.ToList();
            for (var i = _cardOverviews.Count; i < cardsList.Count; i++)
                Add();

            for (var i = 0; i < _cardOverviews.Count; i++)
            {
                if (i < cardsList.Count)
                {
                    _cardOverviews[i].SetTargetCard(cardsList[i]);
                    _cardOverviews[i].Show();
                }
                else
                    _cardOverviews[i].Hide();
            }

            Root.style.display = DisplayStyle.Flex;
            Visible = true;
        }

        public void Hide()
        {
            Root.style.display = DisplayStyle.None;
            Visible = false;
        }

        private void Add()
        {
            var cardOverviewRoot = _cardTree.CloneTree().Q<VisualElement>("Card");
            var cardOverview = new CardOverview(cardOverviewRoot);
            _cardOverviews.Add(cardOverview);
            
            var index = _cardOverviews.Count - 1;
            var halfCardsPerRow = _cardsPerRow / 2;
            _cardOverviews[index].Root.style.width =
                new Length(_maxCardHeight / _cardsPerRow, LengthUnit.Percent);
            _cardOverviews[index].Root.style.height =
                new Length(_maxCardHeight / _cardsPerRow / 3 * 5, LengthUnit.Percent);
            var xTranslate = 
                new Length((100 + _spacing.x) * (index % _cardsPerRow - halfCardsPerRow), LengthUnit.Percent);
            var yTranslate = 
                new Length((100 + _spacing.y) * (index / _cardsPerRow) + _topOffset, LengthUnit.Percent);
            _cardOverviews[index].Root.style.translate = new Translate(xTranslate, yTranslate);
                
            CardsBoard.Add(cardOverview.Root);
        }
    }
}