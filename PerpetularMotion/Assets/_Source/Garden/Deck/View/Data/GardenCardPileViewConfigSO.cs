﻿using UnityEngine;

namespace Garden.Deck.View.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/View/CardPileConfigSO", fileName = "NewCardPileViewConfigSO")]
    public class GardenCardPileViewConfigSO : ScriptableObject
    {
        [field: SerializeField] public int CardsPerRow { get; private set; }
        [field: SerializeField] public Vector2 Spacing { get; private set; }
        [field: SerializeField] public float TopOffset { get; private set; }
        [field: SerializeField] public float MaxCardHeight { get; private set; }
    }
}