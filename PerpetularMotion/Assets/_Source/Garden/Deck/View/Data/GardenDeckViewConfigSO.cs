﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Garden.Deck.View.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/View/DeckConfigSO", fileName = "NewDeckViewConfigSO")]
    public class GardenDeckViewConfigSO : ScriptableObject
    {
        [field: SerializeField] public VisualTreeAsset CardPileTreeAsset { get; private set; }
        [field: SerializeField] public VisualTreeAsset CardTreeAsset { get; private set; }
    }
}