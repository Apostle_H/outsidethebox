﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Garden.Deck.View.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/View/HandConfigSO", fileName = "NewHandViewConfigSO")]
    public class GardenHandViewConfigSO : ScriptableObject
    {
        [field: SerializeField] public VisualTreeAsset CardTreeAsset { get; private set; }
        [field: SerializeField] public Vector2 CardOffset { get; private set; }
        [field: SerializeField] public float MaxRotationDeg { get; private set; }
    }
}