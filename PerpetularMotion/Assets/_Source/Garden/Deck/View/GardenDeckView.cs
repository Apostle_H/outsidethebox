﻿using System;
using System.Collections.Generic;
using Deck;
using Deck.Card;
using Garden.Deck.Card;
using Garden.Deck.View.Data;
using UnityEngine;
using UnityEngine.UIElements;
using VContainer;
using VContainer.Unity;

namespace Garden.Deck.View
{
    public class GardenDeckView : IStartable, IDisposable
    {
        private UIDocument _canvas;
        private VisualTreeAsset _cardPileTreeAsset;
        private VisualTreeAsset _cardTreeAsset;

        private int _cardPileCardsPerRow;
        private Vector2 _cardPileSpacing;
        private float _cardPileTopOffset;
        private float _cardPileMaxCardHeight;
        
        private CardPileView _cardPileView;

        private VisualElement _drawPileToggleBtn; // pile index 0
        private VisualElement _discardPileToggleBtn; // pile index 1
        private VisualElement _lostPileToggleBtn; // pile index 2
        
        private int _pileIndex = -1;
        private Dictionary<int, Func<IEnumerable<IGardenCard>>> _cardsGetters = new();

        public IDeck<IGardenCard> Deck { get; private set; }
        public VisualElement Root { get; private set; }

        [Inject]
        public GardenDeckView(GardenDeckViewConfigSO configSO, UIDocument canvas, IDeck<IGardenCard> deck, 
            GardenCardPileViewConfigSO cardPileViewConfigSO)
        {
            _canvas = canvas;
            _cardPileTreeAsset = configSO.CardPileTreeAsset;
            _cardTreeAsset = configSO.CardTreeAsset;
            
            Deck = deck;

            _cardPileCardsPerRow = cardPileViewConfigSO.CardsPerRow;
            _cardPileSpacing = cardPileViewConfigSO.Spacing;
            _cardPileTopOffset = cardPileViewConfigSO.TopOffset;
            _cardPileMaxCardHeight = cardPileViewConfigSO.MaxCardHeight;
            
            _cardsGetters.Add(0, Deck.GetDrawPile);
            _cardsGetters.Add(1, Deck.GetDiscardPile);
            _cardsGetters.Add(2, Deck.GetLostPile);
        }

        public void Start()
        {
            Root = _canvas.rootVisualElement;
            _drawPileToggleBtn = Root.Q<VisualElement>("DrawPileToggleBtn");
            _discardPileToggleBtn = Root.Q<VisualElement>("DiscardPileToggleBtn");
            _lostPileToggleBtn = Root.Q<VisualElement>("LostPileToggleBtn");

            var cardPileRoot = _cardPileTreeAsset.Instantiate().Q<VisualElement>("CardPile");
            _cardPileView = new CardPileView(_cardPileCardsPerRow, _cardPileSpacing, _cardPileTopOffset, 
                _cardPileMaxCardHeight, cardPileRoot, _cardTreeAsset);
            _cardPileView.Hide();
            Root.Add(_cardPileView.Root);
            
            Bind();
        }

        public void Dispose() => Expose();

        private void Bind()
        {
            _drawPileToggleBtn.RegisterCallback<MouseDownEvent>(ToggleDrawPile);
            _discardPileToggleBtn.RegisterCallback<MouseDownEvent>(ToggleDiscardPile);
            _lostPileToggleBtn.RegisterCallback<MouseDownEvent>(ToggleLostDrawPile);
        }

        private void Expose()
        {
            _drawPileToggleBtn.UnregisterCallback<MouseDownEvent>(ToggleDrawPile);
            _discardPileToggleBtn.UnregisterCallback<MouseDownEvent>(ToggleDiscardPile);
            _lostPileToggleBtn.UnregisterCallback<MouseDownEvent>(ToggleLostDrawPile);
        }
        
        private void ToggleDrawPile(MouseDownEvent evt) => ToggleCardPile(0);

        private void ToggleDiscardPile(MouseDownEvent evt) => ToggleCardPile(1);

        private void ToggleLostDrawPile(MouseDownEvent evt) => ToggleCardPile(2);

        private void ToggleCardPile(int pileIndex)
        {
            if (_cardPileView.Visible && _pileIndex == pileIndex)
                _cardPileView.Hide();
            else
                _cardPileView.Show(_cardsGetters[pileIndex].Invoke());

            _pileIndex = pileIndex;
        }
    }
}