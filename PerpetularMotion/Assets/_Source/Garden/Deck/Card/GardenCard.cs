﻿using System.Collections.Generic;
using System.Linq;
using Garden.Field.Effects;
using UnityEngine;

namespace Garden.Deck.Card
{
    public class GardenCard : IGardenCard
    {
        private List<IFieldEffect> _effects;
        
        public string Title { get; private set; }
        public bool IsLost { get; private set; }

        public Vector2Int[] TargetTilesOffset { get; private set; }

        public GardenCard(string title, List<IFieldEffect> effects, bool isLost, Vector2Int[] targetTilesOffset)
        {
            _effects = effects;
            
            Title = title;
            IsLost = isLost;
            TargetTilesOffset = targetTilesOffset;
        }
        
        public bool IsAppliable(Vector2Int primaryTargetTileIndex)
        {
            var offsetTargetTileIndexes = 
                TargetTilesOffset.Select(offsetIndex => primaryTargetTileIndex + offsetIndex);
            
            return _effects.All(effect => effect.IsAppliable(offsetTargetTileIndexes));
        }

        public void Apply(Vector2Int primaryTargetTileIndex)
        {
            var offsetTargetTileIndexes = 
                TargetTilesOffset.Select(offsetIndex => primaryTargetTileIndex + offsetIndex);
            
            if (_effects.Any(effect => !effect.IsAppliable(offsetTargetTileIndexes)))
                return;
            
            foreach (var effect in _effects)
                effect.Apply(offsetTargetTileIndexes);
        }

        public string GetInfo()
        {
            return "Info";
        }
    }
}