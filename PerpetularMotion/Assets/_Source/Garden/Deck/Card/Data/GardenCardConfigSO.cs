﻿using Garden.Field.Effects;
using UnityEngine;

namespace Garden.Deck.Card.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/Card/ConfigSO", fileName = "NewCardConfigSO")]
    public class GardenCardConfigSO : ScriptableObject
    {
        [field: SerializeField] public string Title { get; private set; }
        [field: SerializeField] public GardenFieldEffectConfigSO[] CardEffectsSO { get; private set; }
        [field: SerializeField] public bool IsLost { get; private set; }
        [field: SerializeField] public Vector2Int[] TargetTilesOffset { get; private set; }
    }
}