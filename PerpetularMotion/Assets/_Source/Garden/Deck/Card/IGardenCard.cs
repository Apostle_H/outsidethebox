﻿using Deck.Card;
using UnityEngine;

namespace Garden.Deck.Card
{
    public interface IGardenCard : ICard<Vector2Int>
    {
        Vector2Int[] TargetTilesOffset { get; }
    }
}