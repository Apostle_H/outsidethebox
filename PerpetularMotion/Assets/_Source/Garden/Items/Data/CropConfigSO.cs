﻿using Inventory.Data;
using UnityEngine;

namespace Garden.Items.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Items/CropConfigSO", fileName = "NewCropConfigSO")]
    public class CropConfigSO : ItemConfigSO
    {
    }
}