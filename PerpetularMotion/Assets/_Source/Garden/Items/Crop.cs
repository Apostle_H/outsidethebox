﻿using System;
using Inventory.Data;
using UnityEngine;

namespace Garden.Items
{
    public class Crop : ICrop
    {
        public int Id { get; }
        public string Title { get; }
        public string Description { get; }
        public Sprite Icon { get; }
        public int Amount { get; private set; }

        public event Action<IItem> OnModify;

        public Crop(int id, string title, string description, Sprite icon)
        {
            Id = id;
            Title = title;
            Description = description;
            Icon = icon;
            Amount = 1;
        }

        public void Modify(int quantity)
        {
            Amount += quantity;

            if (Amount < 0)
                Amount = 0;
            
            OnModify?.Invoke(this);
        }

        public IItem Take(int amount)
        {
            Modify(-amount);

            var newCrop = new Crop(Id, Title, Description, Icon) { Amount = amount };
            return newCrop;
        }
    }
}