﻿using Garden.Field.Effects;
using Garden.Field.Tile.Data.State;
using Garden.Field.Tile.Effects.Runtime;
using UnityEngine;

namespace Garden.Field.Tile.Effects.Configs
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/Card/Effects/TileStateSO", fileName = "NewTileStateCardEffectSO")]
    public class TileStateEffectConfigSO : GardenFieldEffectConfigSO
    {
        [field: SerializeField] public TileStat TargetStat { get; private set; }
        [field: SerializeField] public bool Setter { get; private set; }
        [field: SerializeField] public int Value { get; private set; }

        
        public override IFieldEffect Build() => new TileStateEffect(TargetStat, Setter, Value);
    }
}