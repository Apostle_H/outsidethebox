﻿using System.Collections.Generic;
using System.Linq;
using Field.Grid;
using Garden.Field.Effects;
using Garden.Field.Tile.Data.State;
using UnityEngine;
using VContainer;

namespace Garden.Field.Tile.Effects.Runtime
{
    public class TileStateEffect : IFieldEffect
    {
        private IGrid<IGardenTile> _targetGrid;
        
        private TileStat _targetStat;
        private bool _setter;
        private int _value;   

        public TileStateEffect(TileStat targetStat, bool setter, int value)
        {
            _targetStat = targetStat;
            _setter = setter;
            _value = value;

            _targetGrid = default;
        }

        [Inject]
        private void Inject(IGrid<IGardenTile> targetGrid)
        {
            _targetGrid = targetGrid;
        }

        public bool IsAppliable(IEnumerable<Vector2Int> targetTilesIndexes)
        {
            var targetGrid = _targetGrid;
            
            return targetTilesIndexes.Any(targetTileIndex =>
                targetGrid.GetTile(targetTileIndex, out _));
        }

        public void Apply(IEnumerable<Vector2Int> targetTilesIndexes)
        {
            foreach (var targetTileIndex in targetTilesIndexes)
            {
                if (!_targetGrid.GetTile(targetTileIndex, out var targetTile))
                    continue;

                var delta = _value;
                if (_setter)
                    delta -= targetTile.State[_targetStat];

                switch (_targetStat)
                {
                    case TileStat.WETNESS:
                        targetTile.ModifyWetness(delta);
                        break;
                    case TileStat.RICHNESS:
                        targetTile.ModifyRichness(delta);
                        break;
                    case TileStat.TEMPERATURE:
                        targetTile.ModifyTemperature(delta);
                        break;
                }
            }
        }
    }
}