﻿using System;
using Field.Grid;
using Field.Grid.Tile.TilesEntity;
using Garden.Field.Tile.Data.State;
using UnityEngine;

namespace Garden.Field.Tile
{
    public class GardenGroundTile : IGardenTile
    {
        private TileState _state;
        
        public IGrid<IGardenTile> Owner { get; private set; }
        public TileState State => _state;
        public Vector2Int GridIndex { get; private set; }
        public ITileEntity<IGardenTile> Entity { get; private set; }
        
        public int Time { get; private set; }
        
        public event Action<int> OnUpdateTime;
        public event Action<WetnessState> OnModifyWetness;
        public event Action<RichnessState> OnModifyRichness;
        public event Action<TemperatureState> OnModifyTemperature;
        public event Action<ITileEntity<IGardenTile>> OnInstalledEntity;
        public event Action<ITileEntity<IGardenTile>> OnUninstalledEntity;

        public GardenGroundTile(IGrid<IGardenTile> owner, TileState state, Vector2Int index)
        {
            Owner = owner;
            _state = state;
            GridIndex = index;
        }
        
        public void ModifyWetness(int delta)
        {
            var currentValue = State[TileStat.WETNESS];
            var modified = currentValue + delta;
            if (modified < 0 || !Enum.IsDefined(typeof(WetnessState), modified))
                return;

            _state.WetnessState = (WetnessState)modified;
            OnModifyWetness?.Invoke((WetnessState)State[TileStat.WETNESS]);
        }
        
        public void ModifyRichness(int delta)
        {
            var currentValue = State[TileStat.RICHNESS];
            var modified = currentValue + delta;
            if (modified < 0 || !Enum.IsDefined(typeof(RichnessState), modified))
                return;

            _state.RichnessState = (RichnessState)modified;
            OnModifyRichness?.Invoke((RichnessState)State[TileStat.RICHNESS]);
        }

        public void ModifyTemperature(int delta)
        {
            var currentValue =  State[TileStat.TEMPERATURE];
            var modified = currentValue + delta;
            if (modified < 0 || !Enum.IsDefined(typeof(TemperatureState), modified))
                return;

            _state.TemperatureState = (TemperatureState)modified;
            OnModifyTemperature?.Invoke((TemperatureState)State[TileStat.TEMPERATURE]);
        }

        public void InstallEntity(ITileEntity<IGardenTile> tileEntity)
        {
            if (tileEntity == default)
                return;

            if (Entity != default)
                UninstallEntity();
            
            Entity = tileEntity;
            Entity.SetInstalledTile(this);

            OnInstalledEntity?.Invoke(Entity);
        }

        public void UninstallEntity()
        {
            if (Entity == default)
                return;
            
            var tempEntity = Entity;
            Entity?.SetInstalledTile(default);
            Entity = default;
            
            OnUninstalledEntity?.Invoke(tempEntity);
        }
        
        public void UpdateTime(int delta)
        {
            Time += delta;
            
            Entity?.UpdateTime(delta);
            OnUpdateTime?.Invoke(Time);
        }
    }
}