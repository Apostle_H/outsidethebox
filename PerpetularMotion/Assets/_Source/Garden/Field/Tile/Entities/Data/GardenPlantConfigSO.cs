﻿using Field.Grid.Tile.TilesEntity.Data;
using Garden.Field.Tile.Data.State;
using Garden.Items.Data;
using UnityEngine;

namespace Garden.Field.Tile.Entities.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Field/Tile/Entities/PlantSO", fileName = "NewGardenPlantSO")]
    public class GardenPlantConfigSO : EntityConfigSO
    {
        [field: SerializeField] public string Name { get; private set; }
        [field: SerializeField] public TileState MinimalGrowthTileState { get; private set; }
        [field: SerializeField] public TileState MaximalGrowthTileState { get; private set; }
        [field: SerializeField] public int GrowTime { get; private set; }
        [field: SerializeField] public CropConfigSO CropConfigSO { get; private set; }
    }
}