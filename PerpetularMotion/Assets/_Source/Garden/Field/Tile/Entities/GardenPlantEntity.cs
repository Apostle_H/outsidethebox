﻿using System;
using System.Linq;
using Field.Grid.Tile.TilesEntity;
using Garden.Field.Effects;
using Garden.Field.Tile.Data.State;
using Garden.Items;
using Inventory;
using UnityEngine;
using VContainer;

namespace Garden.Field.Tile.Entities
{
    public class GardenPlantEntity : ITileEntity<IGardenTile>
    {
        private IStorage _storage;

        private IFieldEffect[] _onInstallEffects;
        
        public int TypeId { get; private set; }
        
        public IGardenTile InstalledTile { get; private set; }
        
        public string Name { get; private set; }
        public TileState MinimalGrowthTileState { get; private set; }
        public TileState MaximalGrowthTileState { get; private set; }
        public int LiveTime { get; private set; }
        
        public int CropId { get; private set; }
        public string CropTitle { get; private set; }
        public string CropDescription { get; private set; }
        public Sprite CropIcon { get; private set; }

        public int Time { get; private set; }
        public event Action<int> OnUpdateTime;

        public GardenPlantEntity(int typeId, string name, TileState minimalGrowthTileState, TileState maximalGrowthTileState, 
            int growTime, int cropId, string cropTitle, string cropDescription, Sprite cropIcon)
        {
            TypeId = typeId;
            
            Name = name;
            MinimalGrowthTileState = minimalGrowthTileState;
            MaximalGrowthTileState = maximalGrowthTileState;
            LiveTime = growTime;

            CropId = cropId;
            CropTitle = cropTitle;
            CropIcon = cropIcon;
            CropDescription = cropDescription;
        }

        [Inject]
        private void Inject(IStorage storage) => _storage = storage;

        public void SetInstalledTile(IGardenTile tile) => InstalledTile = tile;
        
        public void Harvest()
        {
            InstalledTile.UninstallEntity();
            
            if (Time != LiveTime)
                return;
            
            if (_storage.ContainsItem(CropId))
                _storage.ModifyItem(CropId, 1);
            else
                _storage.AddItem(new Crop(CropId, CropTitle, CropDescription, CropIcon));
        }

        public void UpdateTime(int delta)
        {
            if (InstalledTile.State[TileStat.WETNESS] < MinimalGrowthTileState[TileStat.WETNESS] ||
                InstalledTile.State[TileStat.WETNESS] > MaximalGrowthTileState[TileStat.WETNESS] ||
                InstalledTile.State[TileStat.RICHNESS] < MinimalGrowthTileState[TileStat.RICHNESS] ||
                InstalledTile.State[TileStat.RICHNESS] > MaximalGrowthTileState[TileStat.RICHNESS] ||
                InstalledTile.State[TileStat.TEMPERATURE] < MinimalGrowthTileState[TileStat.TEMPERATURE] ||
                InstalledTile.State[TileStat.TEMPERATURE] > MaximalGrowthTileState[TileStat.TEMPERATURE])
                return;
            
            Time += delta;
            if (Time > LiveTime)
                Time = LiveTime;
            
            OnUpdateTime?.Invoke(Time);
        }

        public string GetInfo()
        {
            var wetnessSpaces = 15 - MinimalGrowthTileState.WetnessState.ToString().Length;
            var richnessSpaces = 15 - MinimalGrowthTileState.RichnessState.ToString().Length;
            var temperatureSpaces = 15 - MinimalGrowthTileState.TemperatureState.ToString().Length;
            
            Debug.Log($"{wetnessSpaces} - {richnessSpaces} - {temperatureSpaces}");
            
            var title = $"{Name} - {Time}/{LiveTime} -> {CropTitle}\n";
            var minMaxRow = $"MIN{string.Concat(Enumerable.Repeat(" ", 12))}MAX\n";
            
            var wetnessRow = $"{MinimalGrowthTileState.WetnessState}" +
                             $"{string.Concat(Enumerable.Repeat(" ", wetnessSpaces))}" +
                             $"{MaximalGrowthTileState.WetnessState}\n";
            var richnessRow = $"{MinimalGrowthTileState.RichnessState}" +
                             $"{string.Concat(Enumerable.Repeat(" ", richnessSpaces))}" +
                             $"{MaximalGrowthTileState.RichnessState}\n";
            var temperatureRow = $"{MinimalGrowthTileState.TemperatureState}" +
                                 $"{string.Concat(Enumerable.Repeat(" ", temperatureSpaces))}" +
                                 $"{MaximalGrowthTileState.TemperatureState}\n";

            return title + minMaxRow + wetnessRow + richnessRow + temperatureRow;
        }
    }
}