﻿using Garden.Field.Effects;
using Garden.Field.Tile.Entities.Effects.Runtime;
using UnityEngine;

namespace Garden.Field.Tile.Entities.Effects.Configs
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/Card/Effects/HarvestSO", fileName = "NewHarvestFieldEffectConfigSO")]
    public class HarvestFieldEffectConfigSO : GardenFieldEffectConfigSO
    {
        public override IFieldEffect Build() => new HarvestEffect();
    }
}