﻿using System.Collections.Generic;
using System.Linq;
using Field.Grid;
using Garden.Field.Effects;
using UnityEngine;
using VContainer;

namespace Garden.Field.Tile.Entities.Effects.Runtime
{
    public class HarvestEffect : IFieldEffect
    {
        private IGrid<IGardenTile> _targetGrid;
        
        [Inject]
        private void Inject(IGrid<IGardenTile> targetGrid) => _targetGrid = targetGrid;

        public bool IsAppliable(IEnumerable<Vector2Int> targetTilesIndexes)
        {
            var targetGrid = _targetGrid;
            
            return targetTilesIndexes.Any(targetTileIndex =>
                targetGrid.GetTile(targetTileIndex, out var targetTile) && targetTile.Entity != default);
        }

        public void Apply(IEnumerable<Vector2Int> targetTilesIndexes)
        {
            foreach (var targetTileIndex in targetTilesIndexes)
            {
                if (!_targetGrid.GetTile(targetTileIndex, out var offsetTile) || offsetTile.Entity == default)
                    return;
                
                offsetTile.Entity.Harvest();
            }
        }
    }
}