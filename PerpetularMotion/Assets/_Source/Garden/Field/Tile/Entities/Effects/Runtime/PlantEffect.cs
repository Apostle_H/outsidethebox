﻿using System.Collections.Generic;
using System.Linq;
using Field.Grid;
using Garden.Field.Effects;
using Garden.Field.Tile.Data.State;
using UnityEngine;
using VContainer;

namespace Garden.Field.Tile.Entities.Effects.Runtime
{
    public class PlantEffect : IFieldEffect
    {
        private IGrid<IGardenTile> _targetGrid;
        private IObjectResolver _container;
        
        private readonly int _platnId;
        private readonly string _plantName;
        private readonly TileState _plantMinimalGrowthTileState;
        private readonly TileState _plantMaximalGrowthTileState;
        private readonly int _plantGrowthTime;

        private readonly int _cropId;
        private readonly string _cropTitle;
        private readonly string _cropDescription;
        private readonly Sprite _cropIcon;
        
        public PlantEffect(int platnId, string plantName, TileState plantMinimalGrowthTileState, TileState plantMaximalGrowthTileState,
            int plantGrowthTime, int cropId, string cropTitle, string cropDescription, Sprite cropIcon)
        {
            _platnId = platnId;
            _plantName = plantName;
            _plantMinimalGrowthTileState = plantMinimalGrowthTileState;
            _plantMaximalGrowthTileState = plantMaximalGrowthTileState;
            _plantGrowthTime = plantGrowthTime;

            _cropId = cropId;
            _cropTitle = cropTitle;
            _cropDescription = cropDescription;
            _cropIcon = cropIcon;

            _targetGrid = default;
            _container = default;
        }

        [Inject]
        private void Inject(IGrid<IGardenTile> targetGrid, IObjectResolver container)
        {
            _targetGrid = targetGrid;
            _container = container;
        }

        public bool IsAppliable(IEnumerable<Vector2Int> targetTilesIndexes)
        {
            var targetGrid = _targetGrid;
            
            return targetTilesIndexes.All(targetTileIndex =>
                targetGrid.GetTile(targetTileIndex, out var targetTile) && targetTile.Entity == default);
        }

        public void Apply(IEnumerable<Vector2Int> targetTilesIndexes)
        {
            foreach (var targetTileIndex in targetTilesIndexes)
            {
                if (!_targetGrid.GetTile(targetTileIndex, out var targetTile) || targetTile.Entity != default)
                    return;

                var plant = new GardenPlantEntity(_platnId, _plantName, _plantMinimalGrowthTileState, 
                    _plantMaximalGrowthTileState, _plantGrowthTime, _cropId, _cropTitle, _cropDescription, _cropIcon);
                _container.Inject(plant);
                targetTile.InstallEntity(plant);
            }

        }
    }
}