﻿using Garden.Field.Effects;
using Garden.Field.Tile.Entities.Data;
using Garden.Field.Tile.Entities.Effects.Runtime;
using UnityEngine;

namespace Garden.Field.Tile.Entities.Effects.Configs
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/Card/Effects/PlantSO", fileName = "NewPlantCardEffectConfigSO")]
    public class PlantFieldEffectConfigSO : GardenFieldEffectConfigSO
    {
        [field: SerializeField] public GardenPlantConfigSO InstallPlantConfigSO { get; private set; }
        
        public override IFieldEffect Build() => new PlantEffect(InstallPlantConfigSO.Id, InstallPlantConfigSO.Name,
            InstallPlantConfigSO.MinimalGrowthTileState, InstallPlantConfigSO.MaximalGrowthTileState, 
            InstallPlantConfigSO.GrowTime, InstallPlantConfigSO.CropConfigSO.Id,
            InstallPlantConfigSO.CropConfigSO.Title, InstallPlantConfigSO.CropConfigSO.Description, InstallPlantConfigSO.CropConfigSO.Icon);
    }
}