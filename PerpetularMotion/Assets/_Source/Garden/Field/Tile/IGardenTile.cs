﻿using System;
using Field.Grid.Tile;
using Garden.Field.Tile.Data.State;

namespace Garden.Field.Tile
{
    public interface IGardenTile : ITile<IGardenTile>
    {
        TileState State { get; }
        
        event Action<WetnessState> OnModifyWetness;
        event Action<RichnessState> OnModifyRichness;
        event Action<TemperatureState> OnModifyTemperature;
        
        void ModifyWetness(int delta);
        void ModifyRichness(int delta);
        void ModifyTemperature(int delta);
    }
}