﻿namespace Garden.Field.Tile.Data.State
{
    public enum WetnessState
    {
        NONE = -1,
        CRISP = 0,
        DRY = 1,
        MOIST = 2,
        WET = 3,
        SWAMP = 4
    }
}