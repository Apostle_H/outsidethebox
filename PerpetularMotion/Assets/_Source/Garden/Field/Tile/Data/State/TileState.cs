﻿using System;
using UnityEngine;

namespace Garden.Field.Tile.Data.State
{
    [Serializable]
    public struct TileState
    {
        [field: SerializeField] public WetnessState WetnessState { get; set; }
        [field: SerializeField] public RichnessState RichnessState { get; set; }
        [field: SerializeField] public TemperatureState TemperatureState { get; set; }

        public TileState(WetnessState wetnessState = WetnessState.MOIST, 
            RichnessState richnessState = RichnessState.NORM,
            TemperatureState temperatureState = TemperatureState.MILD)
        {
            WetnessState = wetnessState;
            RichnessState = richnessState;
            TemperatureState = temperatureState;
        }

        public int this[TileStat stat]
        {
            get
            {
                var value = -1;
                switch (stat)
                {
                    case TileStat.WETNESS:
                        value = (int)WetnessState;
                        break;
                    case TileStat.RICHNESS:
                        value = (int)RichnessState;
                        break;
                    case TileStat.TEMPERATURE:
                        value = (int)TemperatureState;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(stat), stat, null);
                }

                return value;
            }
        }
    }
}