﻿namespace Garden.Field.Tile.Data.State
{
    public enum RichnessState
    {
        NONE = -1,
        DEAD = 0,
        POOR = 1,
        NORM = 2,
        FERT = 3,
        RICH = 4
    }
}