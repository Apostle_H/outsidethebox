﻿namespace Garden.Field.Tile.Data.State
{
    public enum TemperatureState
    {
        NONE = -1,
        ICY = 0,
        COLD = 1,
        MILD = 2,
        WARM = 3,
        HOT = 4
    }
}