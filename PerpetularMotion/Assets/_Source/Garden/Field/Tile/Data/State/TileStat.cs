﻿namespace Garden.Field.Tile.Data.State
{
    public enum TileStat
    {
        WETNESS = 0,
        RICHNESS = 1,
        TEMPERATURE = 2
    }
}