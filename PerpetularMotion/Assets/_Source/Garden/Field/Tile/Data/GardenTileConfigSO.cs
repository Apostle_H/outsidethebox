﻿using Garden.Field.Tile.Data.State;
using UnityEngine;

namespace Garden.Field.Tile.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Field/Tile/ConfigSO", fileName = "NewGardenTileConfigSO")]
    public class GardenTileConfigSO : ScriptableObject
    {
        [field: SerializeField] public TileState TileState { get; private set; }
    }
}