﻿using System;
using Field.Grid;
using Garden.Field.Tile;

namespace Garden.Field.Weather
{
    public class WeatherResolver
    {
        private WeatherReport _report;

        private IGrid<IGardenTile> _grid;
        
        public WeatherResolver(WeatherReport report, IGrid<IGardenTile> grid)
        {
            _report = report;
            _grid = grid;
        }

        public void Resolve()
        {
            _grid.ForEachTile(BroadcastToTile);
        }

        private void BroadcastToTile(IGardenTile tile)
        {
            BroadcastStat((int)_report.WetnessState, _report.IsWetnessSetter,
                (int)tile.State.WetnessState, tile.ModifyWetness);
            
            BroadcastStat((int)_report.RichnessState, _report.IsRichnessSetter,
                (int)tile.State.RichnessState, tile.ModifyRichness);
            
            BroadcastStat((int)_report.TemperatureState, _report.IsTemperatureSetter,
                (int)tile.State.TemperatureState, tile.ModifyTemperature);
        }

        private void BroadcastStat(int delta, bool isSetter, int difference, Action<int> modifier)
        {
            if (isSetter)
                delta -= difference;
            
            modifier?.Invoke(delta);
        }
    }
}