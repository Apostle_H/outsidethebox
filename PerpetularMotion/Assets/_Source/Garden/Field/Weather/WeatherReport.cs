﻿using Garden.Field.Tile.Data.State;

namespace Garden.Field.Weather
{
    public class WeatherReport
    {
        public int WetnessState { get; private set; } = 0;
        public bool IsWetnessSetter { get; private set; } = false;

        public int RichnessState { get; private set; } = 0;
        public bool IsRichnessSetter { get; private set; } = false;

        public int TemperatureState { get; private set; } = 0;
        public bool IsTemperatureSetter { get; private set; } = false;

        public void Calculate()
        {
            
        }
    }
}