﻿using System;
using System.Net.NetworkInformation;
using Field.Grid.Tile.TilesEntity;
using Garden.Field.Tile;
using Garden.Field.View.Tile.Entity;
using Plugins.Outline;
using UnityEngine;
using Utils.CustomAttributes;
using Utils.Pooling;
using VContainer;

namespace Garden.Field.View.Tile
{
    public class TileView : MonoBehaviour
    {
        [SerializeField] private Outline outline;
        [SerializeField, WorldVector] protected Vector3 tileEntityPosition;
        
        private ISortPool<TileEntityView, int> _entitiesViewsPool;
        
        public IGardenTile TargetTile { get; protected set; }
        public Vector2Int GridIndex { get; protected set; }
        public Vector3 TileEntityPosition => transform.position + tileEntityPosition;

        public TileEntityView TargetTileEntityView { get; protected set; }
        
        public event Action<TileView> OnSelected;
        public event Action<TileView> OnDeselected;
        
        public void Init(ISortPool<TileEntityView, int> entitiesViewsPool, IGardenTile targetTile = default)
        {
            _entitiesViewsPool = entitiesViewsPool;
            
            SetTargetTile(targetTile);
            Deselect();
        }
        
        private void Bind()
        {
            TargetTile.OnInstalledEntity += InstallEntityView;
            TargetTile.OnUninstalledEntity += UninstallEntityView;
        }

        private void Expose()
        {
            TargetTile.OnInstalledEntity -= InstallEntityView;
            TargetTile.OnUninstalledEntity -= UninstallEntityView;
        }

        public void SetTargetTile(IGardenTile tile)
        {
            if (TargetTile != default)
                Expose();
            TargetTile = tile;
            if (TargetTile != default)
            {
                GridIndex = tile.GridIndex;
                Bind();
            }
        }

        public void Select()
        {
            outline.enabled = true;
            
            OnSelected?.Invoke(this);
        }

        public void Deselect()
        {
            outline.enabled = false;
            
            OnDeselected?.Invoke(this);
        }

        private void InstallEntityView(ITileEntity<IGardenTile> tileEntity)
        {
            if (TargetTileEntityView != default)
                UninstallEntityView(TargetTileEntityView.TargetTileEntity);
            
            if (tileEntity == default)
                return;
            
            TargetTileEntityView = _entitiesViewsPool.Get(tileEntity.TypeId);
                
            TargetTileEntityView.SetTargetTileView(this);
            TargetTileEntityView.SetTargetTileEntity(tileEntity);
        }

        private void UninstallEntityView(ITileEntity<IGardenTile> tileEntity)
        {
            if (TargetTileEntityView == default)
                return;
            
            TargetTileEntityView.SetTargetTileView(default);
            TargetTileEntityView.SetTargetTileEntity(default);
            
            _entitiesViewsPool.Put(TargetTileEntityView, TargetTileEntityView.Id);
            TargetTileEntityView = default;
        }
    }
}