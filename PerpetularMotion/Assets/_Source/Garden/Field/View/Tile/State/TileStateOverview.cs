﻿using Field.Grid.Tile.TilesEntity;
using Garden.Field.Tile;
using Garden.Field.Tile.Data.State;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;

namespace Garden.Field.View.Tile.State
{
    public class TileStateOverview : VisualElement
    {
        private int _shortenValues;
        
        public IGardenTile TargetTile { get; private set; }
        
        public VisualElement StatePanel { get; private set; }
        public Label Wetness { get; private set; }
        public Label Richness { get; private set; }
        public Label Temperature { get; private set; }
        
        public VisualElement EntityPanel { get; private set; }
        public Label Growth { get; private set; }
        public Label Info { get; private set; }
        
        public TileStateOverview(int shortenValues)
        {
            name = "TileStateOverview";
            AddToClassList("tile-state-overview");

            StatePanel = new VisualElement() { name = "StatePanel"};
            StatePanel.AddToClassList("state-panel");
            Add(StatePanel);

            Wetness = new Label("---") { name = "WetnessLabel" };
            Wetness.AddToClassList("title");
            Wetness.AddToClassList("wetness");
            StatePanel.Add(Wetness);
            
            Richness = new Label("---") { name = "RichnessLabel" };
            Richness.AddToClassList("title");
            Richness.AddToClassList("richness");
            StatePanel.Add(Richness);
            
            Temperature = new Label("---") { name = "TemperatureLabel" };
            Temperature.AddToClassList("title");
            Temperature.AddToClassList("temperature");
            StatePanel.Add(Temperature);

            EntityPanel = new VisualElement() { name = "EntityPanel" };
            EntityPanel.AddToClassList("entity-panel");
            Add(EntityPanel);
            
            Growth = new Label("---") { name = "GrowthLabel" };
            Growth.AddToClassList("title");
            Growth.AddToClassList("growth");
            EntityPanel.Add(Growth);

            Info = new Label("---") { name = "TextLabel" };
            Info.AddToClassList("text");
            Info.AddToClassList("info");
            EntityPanel.Add(Info);

            EntityPanel.Hide();
            
            _shortenValues = shortenValues;
        }

        public void SetTargetTile(IGardenTile tile)
        {
            if (TargetTile != default)
            {
                Expose();
                if (TargetTile.Entity != default)
                    UninstalledEntity(TargetTile.Entity);
            }

            TargetTile = tile;
            if (TargetTile == default) 
                return;
            
            Bind();
            UpdateState();
            if (TargetTile.Entity != default) 
                InstalledEntity(TargetTile.Entity);
        }

        private void Bind()
        {
            TargetTile.OnModifyWetness += UpdateWetness;
            TargetTile.OnModifyRichness += UpdateRichness;
            TargetTile.OnModifyTemperature += UpdateTemperature;
            TargetTile.OnInstalledEntity += InstalledEntity;
            TargetTile.OnUninstalledEntity += UninstalledEntity;
        }

        private void Expose()
        {
            TargetTile.OnModifyWetness -= UpdateWetness;
            TargetTile.OnModifyRichness -= UpdateRichness;
            TargetTile.OnModifyTemperature -= UpdateTemperature;
            TargetTile.OnInstalledEntity -= InstalledEntity;
            TargetTile.OnUninstalledEntity -= UninstalledEntity;
        }

        private void BindEntity(ITileEntity<IGardenTile> entity) => entity.OnUpdateTime += UpdateEntity;

        private void ExposeEntity(ITileEntity<IGardenTile> entity) => entity.OnUpdateTime -= UpdateEntity;

        public void SetPosition(Vector2 position, LengthUnit lengthUnit = LengthUnit.Pixel)
        {
            var xTranslate = new Length(position.x, lengthUnit);
            var yTranslate = new Length(position.y, lengthUnit);
            style.translate = new Translate(xTranslate, yTranslate);
        }

        private void UpdateState()
        {
            UpdateWetness((WetnessState)TargetTile.State[TileStat.WETNESS]);
            UpdateRichness((RichnessState)TargetTile.State[TileStat.RICHNESS]);
            UpdateTemperature((TemperatureState)TargetTile.State[TileStat.TEMPERATURE]);
        }

        private void UpdateWetness(WetnessState wetnessState) => Wetness.text = wetnessState.Shorten(_shortenValues);

        private void UpdateRichness(RichnessState richnessState) => Richness.text = richnessState.Shorten(_shortenValues);

        private void UpdateTemperature(TemperatureState temperatureState) => Temperature.text = temperatureState.Shorten(_shortenValues);

        private void InstalledEntity(ITileEntity<IGardenTile> tileEntity)
        {
            BindEntity(tileEntity);
            EntityPanel.Show();
            
            Growth.text = $"{tileEntity.Time}/{tileEntity.LiveTime}";
            Info.text = tileEntity.GetInfo();
        }

        private void UninstalledEntity(ITileEntity<IGardenTile> tileEntity)
        {
            ExposeEntity(tileEntity);
            EntityPanel.Hide();
        }
        
        private void UpdateEntity(int delta) => Growth.text = $"{TargetTile.Entity.Time}/{TargetTile.Entity.LiveTime}";
    }
}