﻿using System;
using System.Collections.Generic;
using Garden.Field.View.Data;
using Garden.Field.View.Grid;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;
using VContainer;
using VContainer.Unity;

namespace Garden.Field.View.Tile.State
{
    public class TileStateView : VisualElement, IStartable, IDisposable
    {
        private UIDocument _canvas;

        private GardenGridView _gardenGridView;

        public TileStateOverview HoverStateOverview { get; private set; }
        private Dictionary<Vector2Int, TileStateOverview> _tilesStatesOverviews = new();
        
        [Inject]
        public TileStateView(GardenTileStateViewConfigSO configSO, UIDocument canvas, GardenGridView gardenGridView)
        {
            name = "TileState";
            pickingMode = PickingMode.Ignore;
            AddToClassList("tile-state");
            
            HoverStateOverview = new TileStateOverview(int.MaxValue);
            Add(HoverStateOverview);
            
            _canvas = canvas;
            _gardenGridView = gardenGridView;
        }

        public void Start()
        {
            _canvas.rootVisualElement.Add(this);
            
            Hide();
            Bind();
        }

        public void Dispose() => Expose();

        private void Bind() => _gardenGridView.OnTileViewAdded += AddTileStats;

        private void Expose()
        {
            _gardenGridView.OnTileViewAdded -= AddTileStats;

            foreach (var kvp in _tilesStatesOverviews)
            {
                if (_gardenGridView.GetTileView(kvp.Key, out var tileView))
                    ExposeTileView(tileView);
                
                kvp.Value.SetTargetTile(default);
            }
        }

        private void BindTileView(TileView tileView)
        {
            tileView.OnSelected += Show;
            tileView.OnDeselected += Hide;
        }

        private void ExposeTileView(TileView tileView)
        {
            tileView.OnSelected -= Show;
            tileView.OnDeselected -= Hide;
        }

        private void Show(TileView tileView)
        {
            HoverStateOverview.SetTargetTile(tileView.TargetTile);
            HoverStateOverview.Show();
        }

        private void Hide(TileView _ = default)
        {
            HoverStateOverview.SetTargetTile(default);
            HoverStateOverview.Hide();
        }
 
        private void AddTileStats(TileView tileView)
        {
            BindTileView(tileView);

            // var tileStateOverviewRoot = _tileStateOverviewTree.Instantiate().Q<VisualElement>("TileState");
            // var tileStateOverview = new TileStateOverview(tileStateOverviewRoot, 1);
            // _tilesStatesOverviews.Add(tileView.GridIndex, tileStateOverview);
            // Root.Add(tileStateOverview.Root);
            //
            // tileStateOverview.Hide();
        }

        private void PlaceOverviewOnTile(TileStateOverview tileStateOverview, Vector3 position)
        {
            var worldPos = position + new Vector3(0f, .5f, 0f);
            var screenPosition = Camera.main.WorldToScreenPoint(worldPos);
            var screenPositionClamped = new Vector2(screenPosition.x / Screen.width, screenPosition.y / Screen.height);
            var flippedPosition = new Vector2(screenPositionClamped.x, 1 - screenPositionClamped.y);
            var adjustedPosition = flippedPosition * panel.visualTree.layout.size - layout.size / 2;
            
            tileStateOverview.SetPosition(adjustedPosition);
        }
    }
}