﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utils.Pooling.Data;
using VContainer;
using Object = UnityEngine.Object;

namespace Garden.Field.View.Tile.Entity.Pooling
{
    public class TileEntitiesViewsPoolConfig : ISortPoolConfig<TileEntityView, int>
    {
        private IObjectResolver _container;
        private Transform _holder;

        private Dictionary<int, GameObject> _prefabs = new();
        
        public Func<int, TileEntityView> Factory { get; private set; }
        public Action<TileEntityView> GetCallback { get; private set; }
        public Action<TileEntityView> PutCallback { get; private set; }
        
        [Inject]
        public TileEntitiesViewsPoolConfig(IObjectResolver container, Transform holder)
        {
            _container = container;
            _holder = holder;
            
            var prefabs = Resources.LoadAll<TileEntityView>("Garden/View/TileEntitiesPrefabs");
            foreach (var tileEntityView in prefabs)
            {
                if (_prefabs.ContainsKey(tileEntityView.Id))
                {
                    Debug.LogWarning($"TileEntity Duplicate For Id:{tileEntityView.Id}");
                    continue;
                }

                _prefabs.Add(tileEntityView.Id, tileEntityView.gameObject);
            }
            
            Factory = FactoryMethod;
            GetCallback = item => item.gameObject.SetActive(true);
            PutCallback = item => item.gameObject.SetActive(false);
        }

        private TileEntityView FactoryMethod(int sortValue)
        {
            if (!_prefabs.ContainsKey(sortValue))
                throw new ArgumentOutOfRangeException($"Tile Entity View Prefab For Id:{sortValue} Was Not Porvided");
            
            var tileEntityViewInstance = Object.Instantiate(_prefabs[sortValue], _holder);
            var tileEntityView = tileEntityViewInstance.GetComponent<TileEntityView>();
            _container.Inject(tileEntityView);

            return tileEntityView;
        }
    }
}