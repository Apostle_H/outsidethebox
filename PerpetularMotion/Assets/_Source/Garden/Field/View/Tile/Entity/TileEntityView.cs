﻿using System.Security.Principal;
using Field.Grid.Tile.TilesEntity;
using Field.Grid.Tile.TilesEntity.Data;
using Garden.Field.Tile;
using UnityEngine;

namespace Garden.Field.View.Tile.Entity
{
    public class TileEntityView : MonoBehaviour
    {
        [SerializeField] private EntityConfigSO entityConfigSO;

        public int Id => entityConfigSO.Id;
        
        public TileView TargetTileView { get; private set; }
        public ITileEntity<IGardenTile> TargetTileEntity { get; private set; }

        public void SetTargetTileView(TileView tileView)
        {
            TargetTileView = tileView;

            if (tileView == default)
                return;
            
            transform.position = TargetTileView.TileEntityPosition;
        }
        
        public void SetTargetTileEntity(ITileEntity<IGardenTile> tileEntity) => TargetTileEntity = tileEntity;
    }
}