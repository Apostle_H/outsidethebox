﻿using System;
using System.Collections.Generic;
using Field.Grid;
using Field.Grid.Selector;
using Garden.Field.Tile;
using Garden.Field.View.Data;
using Garden.Field.View.Tile;
using Garden.Field.View.Tile.Entity;
using Garden.Field.View.Tile.State;
using UnityEngine;
using Utils.Extensions;
using Utils.Pooling;
using VContainer;

namespace Garden.Field.View.Grid
{
    public class GardenGridView : MonoBehaviour
    {
        [SerializeField] private Transform tilesParent;
        
        private GameObject _tilePrefab;
        private Vector2 _snapping;
        
        private IGridTileSelector _gridTileSelector;
        private TileStateView _tileStateView;

        private ISortPool<TileEntityView, int> _tileEntitiesViewsPool;
        
        private Dictionary<Vector2Int, TileView> _tilesViews = new();

        private Vector2Int _currentSelectedIndex;
        
        public IGrid<IGardenTile> TargetGrid { get; private set; }
        
        public event Action<TileView> OnTileViewAdded;

        [Inject]
        private void Inject(GardenGridViewConfigSO configSO, IGrid<IGardenTile> targetGrid, IGridTileSelector gridTileSelector, 
            ISortPool<TileEntityView, int> tileEntitiesViewsPool)
        {
            _tilePrefab = configSO.TilePrefab;
            _snapping = configSO.Snapping;
            
            TargetGrid = targetGrid;
            _gridTileSelector = gridTileSelector;
            
            _tileEntitiesViewsPool = tileEntitiesViewsPool;
            
            if (!_tilePrefab.TryGetComponent(out TileView _))
                throw new ArgumentException("Tile Prefab Lacks MonoTile Component");
        }
        
        private void Start() => Bind();

        private void OnDestroy() => Expose();

        private void Bind()
        {
            TargetGrid.OnTileAdded += AddTileView;
            _gridTileSelector.OnTileIndexSelected += SelectTile;
        }

        private void Expose()
        {
            TargetGrid.OnTileAdded -= AddTileView;
            _gridTileSelector.OnTileIndexSelected -= SelectTile;
        }

        public bool GetTileView(Vector2Int index, out TileView tileView)
        {
            tileView = default;
            if (!_tilesViews.ContainsKey(index))
                return false;

            tileView = _tilesViews[index];
            return true;
        }

        private void AddTileView(IGardenTile tile)
        {
            var index = tile.GridIndex;
            if (_tilesViews.ContainsKey(index))
                _tilesViews[index].SetTargetTile(tile);
            
            var position = PositionWithOffset(index.x, index.y).ReplaceY(_tilePrefab.transform.position);
            var targetGO = Instantiate(_tilePrefab, position, Quaternion.identity, tilesParent);
            var tileView = targetGO.GetComponent<TileView>();

            tileView.Init(_tileEntitiesViewsPool, tile);
            _tilesViews[index] = tileView;
            
            OnTileViewAdded?.Invoke(tileView);
        }

        private void SelectTile(Vector2Int tileIndex)
        {
            if (_tilesViews.ContainsKey(_currentSelectedIndex))
                _tilesViews[_currentSelectedIndex].Deselect();
            
            _currentSelectedIndex = tileIndex;
            
            if (_tilesViews.ContainsKey(tileIndex))
                _tilesViews[_currentSelectedIndex].Select();
        }

        private Vector3 PositionWithOffset(int x, int y)
        {
            var xPos = (1 + 2 * x) * _snapping.x;
            var yPos = (1 + 2 * y) * _snapping.y;
            return new Vector3(xPos, 0, yPos);
        }
    }
}