﻿using System;
using Field.Grid.Selector;
using Garden.Field.View.Data;
using InputSystem.Garden;
using UnityEngine;
using UnityEngine.InputSystem;
using Utils.Extensions;
using VContainer.Unity;

namespace Garden.Field.View.Grid
{
    public class GridTileSelectorView : IStartable, IDisposable
    {
        private GardenActions _actions;

        private IGridTileSelector _tileSelector;

        private Vector2 _gridSnapping;

        public GridTileSelectorView(GardenActions actions, IGridTileSelector tileSelector, GardenGridViewConfigSO gridViewConfigSO)
        {
            _actions = actions;
            _tileSelector = tileSelector;
            _gridSnapping = gridViewConfigSO.Snapping;
        }

        public void Start() => Bind();

        public void Dispose() => Expose();

        private void Bind() => _actions.Main.MousePosition.performed += UpdateSelectedTileIndex;

        private void Expose() => _actions.Main.MousePosition.performed -= UpdateSelectedTileIndex;

        private void UpdateSelectedTileIndex(InputAction.CallbackContext ctx)
        {
            var screenPosition = ctx.ReadValue<Vector2>();
            var worldPosition = Camera.main.ScreenPositionToPositionOnPlane(screenPosition, 0f);
            var gridTileIndex = worldPosition.ToXZ().SnapVector(_gridSnapping);

            if (_tileSelector.SelectedTileIndex == gridTileIndex)
                return;
            
            _tileSelector.SelectIndex(gridTileIndex);
        }
    }
}