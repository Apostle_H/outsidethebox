﻿using UnityEngine;

namespace Garden.Field.View.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Field/View/GridConfigSO", fileName = "NewGridViewConfigSOs")]
    public class GardenGridViewConfigSO : ScriptableObject
    {
        [field: SerializeField] public GameObject TilePrefab { get; private set; }
        [field: SerializeField] public Vector2 Snapping { get; private set; }
    }
}