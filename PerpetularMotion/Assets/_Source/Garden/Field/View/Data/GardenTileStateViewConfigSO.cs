﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Garden.Field.View.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Field/View/TileStateConfigSO", fileName = "NewTileStateViewConfigSO")]
    public class GardenTileStateViewConfigSO : ScriptableObject
    {
        [field: SerializeField] public VisualTreeAsset TileStateOverviewTree { get; private set; }
    }
}