﻿using Garden.Field.Weather;
using StateMachine;
using StateMachine.States;

namespace Garden.Field.StateMachine.States
{
    public class GardenFieldCalculateWeatherState : IState
    {
        private WeatherResolver _weatherResolver;
        private WeatherReport _weatherReport;
        
        public IStateMachine Owner { get; set; }

        public GardenFieldCalculateWeatherState(WeatherResolver weatherResolver, WeatherReport weatherReport)
        {
            _weatherResolver = weatherResolver;
            _weatherReport = weatherReport;
        }

        public void Enter()
        {
            _weatherResolver.Resolve();
            _weatherReport.Calculate();
            
            Owner.Switch<GardenFieldGridUpdateState>();
        }

        public void Update() { }

        public void Exit() { }
    }
}