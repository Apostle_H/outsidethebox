﻿using Garden.Field.Grid;
using StateMachine;
using StateMachine.States;

namespace Garden.Field.StateMachine.States
{
    public class GardenFieldInitializeState : IState
    {
        private GardenGridBuilder _gridBuilder;
        
        public IStateMachine Owner { get; set; }

        public GardenFieldInitializeState(GardenGridBuilder gridBuilder) => _gridBuilder = gridBuilder;

        public void Enter() => _gridBuilder.InitGrid();

        public void Update() { }

        public void Exit() { }
    }
}