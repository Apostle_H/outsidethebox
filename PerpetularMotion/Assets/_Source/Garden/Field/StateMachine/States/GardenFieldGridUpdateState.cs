﻿using Field.Grid;
using Garden.Field.Tile;
using StateMachine;
using StateMachine.States;
using Time.Flow;
using UnityEngine;

namespace Garden.Field.StateMachine.States
{
    public class GardenFieldGridUpdateState : IState
    {
        private ITimeUpdater _timeUpdater;
        private IGrid<IGardenTile> _grid;

        private int _previousRegisteredTime;
        private int _timeDelta;
        
        public IStateMachine Owner { get; set; }

        public GardenFieldGridUpdateState(ITimeUpdater timeUpdater, IGrid<IGardenTile> grid)
        {
            _timeUpdater = timeUpdater;
            _grid = grid;
        }

        public void Enter()
        {
            _timeDelta = _timeUpdater.Time - _previousRegisteredTime;
            _grid.ForEachTile(UpdateTileTime);
            
            Owner.Switch<WaitingState>();
        }

        public void Update() { }

        public void Exit() => _previousRegisteredTime = _timeUpdater.Time;

        private void UpdateTileTime(IGardenTile tile) => tile.UpdateTime(_timeDelta);
    }
}