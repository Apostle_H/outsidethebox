﻿using System;
using System.Collections.Generic;
using Garden.Field.StateMachine.StatesProvider;
using StateMachine;
using StateMachine.States;
using VContainer;
using VContainer.Unity;

namespace Garden.Field.StateMachine
{
    public class GardenFieldStateMachine : IStateMachine, IPostStartable, ITickable
    {
        private Dictionary<Type, IState> _states;
        private IState _currentState;

        [Inject]
        public GardenFieldStateMachine(IGardenFieldStatesProvider statesProvider)
        {
            _currentState = statesProvider.StartingState;

            _states = statesProvider.GetStates();
            foreach (var kvp in _states)
                kvp.Value.Owner = this;
        }
        
        public void PostStart() => _currentState.Enter();

        public void Switch<T>() where T : IState
        {
            var nextState = typeof(T);
            if (!_states.ContainsKey(nextState))
                return;
            
            _currentState?.Exit();
            _currentState = _states[nextState];
            _currentState.Enter();
        }

        public void Tick() => _currentState?.Update();
    }
}