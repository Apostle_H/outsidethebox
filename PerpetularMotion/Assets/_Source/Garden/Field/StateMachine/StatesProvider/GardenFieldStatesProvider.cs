﻿using System;
using System.Collections.Generic;
using System.Linq;
using Garden.Field.StateMachine.States;
using StateMachine.States;
using VContainer;

namespace Garden.Field.StateMachine.StatesProvider
{
    public class GardenFieldStatesProvider : IGardenFieldStatesProvider
    {
        public IState StartingState { get; }
        
        private readonly Dictionary<Type, IState> _states = new();

        [Inject]
        public GardenFieldStatesProvider(WaitingState waiting, GardenFieldInitializeState initialize, 
            GardenFieldCalculateWeatherState calculateWeather, GardenFieldGridUpdateState gridUpdate)
        {
            StartingState = waiting;
            
            _states.Add(waiting.GetType(), waiting);
            _states.Add(initialize.GetType(), initialize);
            _states.Add(calculateWeather.GetType(), calculateWeather);
            _states.Add(gridUpdate.GetType(), gridUpdate);
        }
        
        public Dictionary<Type, IState> GetStates() => 
            _states.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
    }
}