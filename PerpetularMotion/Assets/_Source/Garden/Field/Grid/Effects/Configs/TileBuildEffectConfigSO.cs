﻿using Garden.Field.Effects;
using Garden.Field.Grid.Effects.Runtime;
using Garden.Field.Tile.Data;
using UnityEngine;

namespace Garden.Field.Grid.Effects.Configs
{
    [CreateAssetMenu(menuName = "SO/Garden/Deck/Card/Effects/TileBuildSO", fileName = "NewTileBuildCardEffectSO")]
    public class TileBuildEffectConfigSO : GardenFieldEffectConfigSO
    {
        [field: SerializeField] public GardenTileConfigSO BuildTileConfigSO { get; private set; }
        
        public override IFieldEffect Build() => new TileBuildEffect(BuildTileConfigSO);
    }
}