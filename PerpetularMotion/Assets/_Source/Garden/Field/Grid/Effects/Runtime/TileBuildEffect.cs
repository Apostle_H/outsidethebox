﻿using System.Collections.Generic;
using Field.Grid;
using Garden.Field.Effects;
using Garden.Field.Tile;
using Garden.Field.Tile.Data;
using Garden.Field.Tile.Data.State;
using UnityEngine;
using VContainer;

namespace Garden.Field.Grid.Effects.Runtime
{
    public class TileBuildEffect : IFieldEffect
    {
        private IGrid<IGardenTile> _targetGrid;
        private GardenGridBuilder _gridBuilder;
        
        private GardenTileConfigSO _tileConfigSO;
        
        public TileBuildEffect(GardenTileConfigSO tileConfigSO)
        {
            _tileConfigSO = tileConfigSO;

            _targetGrid = default;
            _gridBuilder = default;
        }

        [Inject]
        private void Inject(IGrid<IGardenTile> targetGrid, GardenGridBuilder gridBuilder)
        {
            _targetGrid = targetGrid;
            _gridBuilder = gridBuilder;
        }

        public bool IsAppliable(IEnumerable<Vector2Int> targetTilesIndexes) => true;

        public void Apply(IEnumerable<Vector2Int> targetTilesIndexes)
        {
            foreach (var targetTileIndex in targetTilesIndexes)
            {
                if (_targetGrid.GetTile(targetTileIndex, out var offsetTile))
                {
                    var delta = _tileConfigSO.TileState[TileStat.WETNESS] - offsetTile.State[TileStat.WETNESS];
                    offsetTile.ModifyWetness(delta);
                    delta = _tileConfigSO.TileState[TileStat.RICHNESS] - offsetTile.State[TileStat.RICHNESS];
                    offsetTile.ModifyRichness(delta);
                    delta = _tileConfigSO.TileState[TileStat.TEMPERATURE] - offsetTile.State[TileStat.TEMPERATURE];
                    offsetTile.ModifyTemperature(delta);
                }
                else
                    _gridBuilder.Build(_tileConfigSO, targetTileIndex);
            }
        }
    }
}