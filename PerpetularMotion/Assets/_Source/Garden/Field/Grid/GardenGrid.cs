﻿using System;
using System.Collections.Generic;
using Field.Grid;
using Garden.Field.Tile;
using Time.Flow;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Garden.Field.Grid
{
    public class GardenGrid : IGrid<IGardenTile>
    {
        private readonly Dictionary<Vector2Int, IGardenTile> _tiles = new();
        
        public event Action<IGardenTile> OnTileAdded;

        public void AddTile(IGardenTile tile, Vector2Int index)
        {
            _tiles.Add(index, tile);
            
            OnTileAdded?.Invoke(tile);
        }

        public bool GetTile(Vector2Int index, out IGardenTile tile)
        {
            tile = default;
            if (!_tiles.ContainsKey(index))
                return false;
            
            tile = _tiles[index];
            return true;
        }

        public void ForEachTile(Action<IGardenTile> action)
        {
            if (action == default)
                return;
            
            foreach (var kvp in _tiles)
                action.Invoke(kvp.Value);
        }
    }
}