﻿using Field.Grid;
using Garden.Field.Grid.Data;
using Garden.Field.Tile;
using Garden.Field.Tile.Data;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Garden.Field.Grid
{
    public class GardenGridBuilder
    {
        private IObjectResolver _container;
        
        private GardenGridTile[] _tiles;
        private IGrid<IGardenTile> _grid;

        public GardenGridBuilder(IObjectResolver container, GardenGridConfigSO configSO, IGrid<IGardenTile> grid)
        {
            _container = container;
            _tiles = configSO.Tiles;
            _grid = grid;
        }
        
        public void InitGrid()
        {
            foreach (var gridTile in _tiles) 
                Build(gridTile.TileConfigSO, gridTile.Index);
        }

        public void Build(GardenTileConfigSO gardenTileConfigSO, Vector2Int index)
        {
            if (_grid.GetTile(index, out _))
                return;

            var tile = new GardenGroundTile(_grid, gardenTileConfigSO.TileState, index);
            _container.Inject(tile);
            
            _grid.AddTile(tile, index);
        }
    }
}