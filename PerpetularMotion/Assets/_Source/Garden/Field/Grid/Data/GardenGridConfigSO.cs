﻿using UnityEngine;

namespace Garden.Field.Grid.Data
{
    [CreateAssetMenu(menuName = "SO/Garden/Field/Grid/ConfigSO", fileName = "NewGridConfigSO")]
    public class GardenGridConfigSO : ScriptableObject
    {
        [field: SerializeField] public GardenGridTile[] Tiles { get; private set; }
    }
}