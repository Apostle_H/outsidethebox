﻿using System;
using Garden.Field.Tile.Data;
using UnityEngine;

namespace Garden.Field.Grid.Data
{
    [Serializable]
    public class GardenGridTile
    {
        [field: SerializeField] public GardenTileConfigSO TileConfigSO { get; private set; }
        [field: SerializeField] public Vector2Int Index { get; private set; }
    }
}