﻿using UnityEngine;

namespace Garden.Field.Effects
{
    public abstract class GardenFieldEffectConfigSO : ScriptableObject
    {
        public abstract IFieldEffect Build();
    }
}