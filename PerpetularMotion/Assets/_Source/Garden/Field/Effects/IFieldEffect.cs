﻿using System.Collections.Generic;
using Effect.Data;
using UnityEngine;

namespace Garden.Field.Effects
{
    public interface IFieldEffect : IEffect<IEnumerable<Vector2Int>>
    {
        
    }
}