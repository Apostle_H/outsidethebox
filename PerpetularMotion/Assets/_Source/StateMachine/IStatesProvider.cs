﻿using System;
using System.Collections.Generic;
using StateMachine.States;

namespace StateMachine
{
    public interface IStatesProvider
    {
        IState StartingState { get; }
        
        Dictionary<Type, IState> GetStates();
    }
}