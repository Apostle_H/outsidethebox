﻿namespace StateMachine.States
{
    public interface IState
    {
        IStateMachine Owner { get; set; }
        
        void Enter();
        void Update();
        void Exit();
    }
}