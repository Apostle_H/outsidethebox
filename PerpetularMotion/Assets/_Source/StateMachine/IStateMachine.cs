﻿using System;
using StateMachine.States;
using VContainer.Unity;

namespace StateMachine
{
    public interface IStateMachine : IPostStartable
    {
        void Switch<T>() where T : IState; 
    }
}