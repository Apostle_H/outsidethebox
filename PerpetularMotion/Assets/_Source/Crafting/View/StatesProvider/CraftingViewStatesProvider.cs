﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crafting.Potion.View;
using StateMachine.States;
using VContainer;

namespace Crafting.View.StatesProvider
{
    public class CraftingViewStatesProvider : ICraftingViewStatesProvider
    {
        public IState StartingState { get; }

        private readonly Dictionary<Type, IState> _states = new();

        [Inject]
        public CraftingViewStatesProvider(PotionView potionView)
        {
            StartingState = potionView;
            
            _states.Add(potionView.GetType(), potionView);
        }
        
        public Dictionary<Type, IState> GetStates() => 
            _states.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
    }
}