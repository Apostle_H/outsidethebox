﻿using System;
using System.Collections.Generic;
using Crafting.View.StatesProvider;
using StateMachine;
using StateMachine.States;
using UnityEngine.UIElements;
using Utils.Extensions;
using VContainer;
using VContainer.Unity;

namespace Crafting.View
{
    public class CraftingView : VisualElement, IStateMachine, IStartable, IDisposable
    {
        private UIDocument _canvas;
     
        private readonly Dictionary<Type, IState> _states;
        private IState _currentState;
        
        private bool _isOpen = true;
        
        public VisualElement Toggle { get; private set; }
        public VisualElement CraftingPanel { get; private set; }

        [Inject]
        public CraftingView(UIDocument canvas, ICraftingViewStatesProvider statesProvider)
        {
            _canvas = canvas;
            
            name = "Crafting";
            AddToClassList("crafting");
            pickingMode = PickingMode.Ignore;
            
            CraftingPanel = new VisualElement() { name = "CraftingPanel" };
            CraftingPanel.AddToClassList("crafting-panel");
            Add(CraftingPanel);

            Toggle = new VisualElement() { name = "CraftingToggle" };
            Toggle.AddToClassList("toggle");
            Add(Toggle);
            
            _currentState = statesProvider.StartingState;
            
            _states = statesProvider.GetStates();
            foreach (var kvp in _states)
            {
                kvp.Value.Owner = this;
                if (kvp.Value is not VisualElement view) 
                    continue;
                
                CraftingPanel.Add(view);
                view.Hide();
            }

        }

        public void Start()
        {
            _canvas.rootVisualElement.Add(this);
            
            ToggleCrafting();
            Bind();
        }

        public void PostStart()
        {
            _currentState?.Enter();
        }
        
        public void Switch<T>() where T : IState
        {
            var nextState = typeof(T);
            if (!_states.ContainsKey(nextState))
                return;
            
            _currentState?.Exit();
            _currentState = _states[nextState];
            _currentState.Enter();
        }

        public void Dispose() => Expose();

        private void Bind() => Toggle.RegisterCallback<MouseDownEvent>(ToggleCraftingBtn);

        private void Expose() => Toggle.RegisterCallback<MouseDownEvent>(ToggleCraftingBtn);

        private void ToggleCraftingBtn(MouseDownEvent evt) => ToggleCrafting();

        private void ToggleCrafting()
        {
            _isOpen = !_isOpen;
            
            CraftingPanel.Toggle(_isOpen);
        }
    }
}