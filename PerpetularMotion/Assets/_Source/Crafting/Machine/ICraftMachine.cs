﻿using System.Collections.Generic;
using Crafting.Data;
using Inventory;
using Inventory.Data;

namespace Crafting.Machine
{
    public interface ICraftMachine<T> where T : IItem
    {
        bool Craft(IStorage storage, out T result);
        bool Craft(ICraftRecipe<T> recipe, IStorage storage, out T result);
    }
}