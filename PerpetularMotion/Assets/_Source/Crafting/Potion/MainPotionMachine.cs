﻿using System.Collections.Generic;
using System.Linq;
using Crafting.Data;
using Crafting.Potion.Data;
using Effect.Data;
using Inventory;
using Inventory.Data;
using Utils.Extensions;
using VContainer;

namespace Crafting.Potion
{
    public class MainPotionMachine : IPotionMachine
    {
        private IObjectResolver _container;
        private PotionMachineRecipesSO _recipesSO;

        [Inject]
        private void Inject(IObjectResolver container, PotionMachineRecipesSO recipesSO)
        {
            _container = container;
            _recipesSO = recipesSO;
        }

        public bool Craft(IStorage storage, out IPotion result)
        {
            result = default;
            if (!CompareItemsToRecipes(storage.ToItems(), out var matchingRecipe))
                return false;

            Craft(matchingRecipe, storage, out result);
            return true;
        }

        public bool Craft(ICraftRecipe<IPotion> recipe, IStorage storage, out IPotion result)
        {
            result = default;
            bool canCraft = recipe.Ingredients.All(recipeIngredient => 
                storage.CountType(recipeIngredient.Id) >= recipeIngredient.Quantity);

            if (!canCraft)
                return false;

            var potionEffects = new List<IEffect>();
            var potionConfigSO = (PotionConfigSO)recipe.Result;
            foreach (var potionEffectSO in potionConfigSO.PotionEffectsSO)
            {
                var potionEffect = potionEffectSO.Build();
                _container.Inject(potionEffect);
                
                potionEffects.Add(potionEffect);
            }
            
            result = new Data.Potion(potionConfigSO.Id, potionConfigSO.Title, potionConfigSO.Description, potionConfigSO.Icon, potionEffects.ToArray());
            _container.Inject(result);
            
            foreach (var ingredient in recipe.Ingredients)
                storage.ModifyItem(ingredient.Id, -ingredient.Quantity);
            
            storage.AddItem(result);
            
            return true;
        }

        private bool CompareItemsToRecipes(IEnumerable<IItem> items, out ICraftRecipe<IPotion> matchingRecipe)
        {
            matchingRecipe = default;
            
            var itemsArray = items.OrderBy(item => item.Id).ToArray();
            foreach (var recipe in _recipesSO.Recipes)
            {
                if (itemsArray.Length != recipe.Ingredients.Length)
                    continue;

                if (!recipe.Ingredients.CompareSorted(itemsArray, CompareItemToRecipe)) 
                    continue;
                
                matchingRecipe = recipe;
                return true;
            }

            return false;
        }

        private bool CompareItemToRecipe(CraftIngredient ingredient, IItem item) => 
            item.Id == ingredient.Id && item.Amount == ingredient.Quantity;
    }
}