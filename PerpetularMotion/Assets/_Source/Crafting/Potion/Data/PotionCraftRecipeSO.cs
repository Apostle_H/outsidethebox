﻿using System;
using System.Linq;
using Crafting.Data;
using Inventory.Data;
using UnityEngine;

namespace Crafting.Potion.Data
{
    [CreateAssetMenu(menuName = "SO/Crafting/Potion/CraftRecipeSO", fileName = "NewPotionCraftRecipeSO")]
    public class PotionCraftRecipeSO : ScriptableObject, ICraftRecipe<IPotion>
    {
        [field: SerializeField] public CraftIngredient[] Ingredients { get; private set; }
        [field: SerializeField] public PotionConfigSO ResultPotion { get; private set; }
        public ItemConfigSO Result => ResultPotion;

        public int Id => GetInstanceID();

        private void OnValidate()
        {
            try
            {
                Ingredients = Ingredients.OrderBy(ingredient => ingredient.Id).ToArray();
            }
            catch (NullReferenceException e)
            {
                Debug.Log("Make Sure To Set All Item Configs For Ingredients");
            }
        }
    }
}