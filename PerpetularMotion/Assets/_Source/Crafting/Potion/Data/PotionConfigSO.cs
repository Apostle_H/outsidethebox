﻿using Crafting.Potion.Data.Effects;
using Inventory.Data;
using UnityEngine;

namespace Crafting.Potion.Data
{
    [CreateAssetMenu(menuName = "SO/Crafting/Potion/ConfigSO", fileName = "NewPotionConfigSO")]
    public class PotionConfigSO : ItemConfigSO
    {
        [field: SerializeField] public PotionEffectConfigSO[] PotionEffectsSO { get; private set; }
    }
}