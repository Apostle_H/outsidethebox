﻿using UnityEngine;

namespace Crafting.Potion.Data
{
    [CreateAssetMenu(menuName = "SO/Crafting/Potion/PotionMachineRecipesSO", fileName = "PotionMachineRecipesSO")]
    public class PotionMachineRecipesSO : ScriptableObject
    {
        [field: SerializeField] public PotionCraftRecipeSO[] Recipes { get; private set; }
    }
}