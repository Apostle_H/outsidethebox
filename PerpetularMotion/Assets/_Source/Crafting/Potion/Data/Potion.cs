﻿using System;
using System.Collections.Generic;
using System.Linq;
using Effect.Data;
using Inventory.Data;
using UnityEngine;

namespace Crafting.Potion.Data
{
    public class Potion : IPotion
    {
        private IEffect[] _effects;
        
        public int Id { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }
        public int Amount { get; private set; }
        public Sprite Icon { get; private set; }
        
        public event Action<IItem> OnModify;

        public Potion(int id, string title, string description, Sprite icon, IEffect[] effects)
        {
            _effects = effects;
            
            Id = id;
            Title = title;
            Description = description;
            Icon = icon;
            Amount = 1;
        }

        public void Modify(int quantity)
        {
            Amount += quantity;

            if (Amount < 0)
                Amount = 0;
            
            OnModify?.Invoke(this);
        }
        
        public IItem Take(int amount)
        {
            Modify(-amount);

            var newPotion = new Potion(Id, Title, Description, Icon, _effects.ToArray()) { Amount = amount };
            return newPotion;
        }
    }
}