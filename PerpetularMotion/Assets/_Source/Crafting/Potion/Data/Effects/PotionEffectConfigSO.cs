﻿using Effect.Data;
using UnityEngine;

namespace Crafting.Potion.Data.Effects
{
    public abstract class PotionEffectConfigSO : ScriptableObject
    {
        public abstract IEffect Build();
    }
}