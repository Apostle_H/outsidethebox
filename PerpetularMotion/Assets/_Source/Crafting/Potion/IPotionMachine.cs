﻿using Crafting.Machine;
using Crafting.Potion.Data;

namespace Crafting.Potion
{
    public interface IPotionMachine : ICraftMachine<IPotion>
    {
        
    }
}