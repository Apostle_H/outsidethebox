﻿using System.Collections.Generic;
using Inventory;
using Inventory.Data;
using Inventory.View;
using StateMachine;
using StateMachine.States;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;
using VContainer;

namespace Crafting.Potion.View
{
    public class PotionView : VisualElement, IState
    {
        private IPotionMachine _craftMachine;

        private Storage _ingredients = new();
        private List<ItemSlotView> _ingredientsSlotsViews = new();
        
        public IStateMachine Owner { get; set; }
        
        public VisualElement IngredientsSlotsViewsHolder { get; private set; }
        public ItemSlotView FluidSlotView { get; private set; }
        public Button MixIngredientsBtn { get; private set; }
        public ItemSlotView ResultSlotView { get; private set; }
        
        [Inject]
        public PotionView(IPotionMachine craftMachine)
        {
            _craftMachine = craftMachine;
            
            name = "PotionCrafting";
            AddToClassList("potion-crafting");
            pickingMode = PickingMode.Ignore;

            IngredientsSlotsViewsHolder = new VisualElement() { name = "IngredientsHolder"};
            IngredientsSlotsViewsHolder.AddToClassList("ingredients-holder");
            Add(IngredientsSlotsViewsHolder);
            
            // FluidSlotView = new ItemSlotView();
            // FluidSlotView.SetTargetSlot(new ItemSlot());
            // AddToClassList("ingredient");

            MixIngredientsBtn = new Button();
            MixIngredientsBtn.AddToClassList("btn");
            MixIngredientsBtn.AddToClassList("mix");
            MixIngredientsBtn.AddToClassList("title");
            MixIngredientsBtn.text = "Mix";
            Add(MixIngredientsBtn);

            ResultSlotView = new ItemSlotView();
            ResultSlotView.SetTargetSlot(new ItemSlot());
            ResultSlotView.AddToClassList("result");
            Add(ResultSlotView);
            
            InitSlots();
        }

        private void InitSlots()
        {
            for (var i = 0; i < 3; i++)
            {
                var itemSlotView = new ItemSlotView(_ingredients.AddEmptySlot());
                _ingredientsSlotsViews.Add(itemSlotView);
                IngredientsSlotsViewsHolder.Add(itemSlotView);
            }
        }

        public void Enter()
        {
            Bind();
            this.Show();
        }

        public void Update() { }

        public void Exit()
        {
            Expose();
            this.Hide();
        }

        private void Bind() => MixIngredientsBtn.clicked += MixIngredients;

        private void Expose() => MixIngredientsBtn.clicked -= MixIngredients;

        private void MixIngredients()
        {
            var hasResult = _craftMachine.Craft(_ingredients, out var potion);
            _ingredients.Clear();
            
            if (!hasResult)
                return;
            
            ResultSlotView.TargetSlot.SetStoredItem(potion);
        }
    }
}