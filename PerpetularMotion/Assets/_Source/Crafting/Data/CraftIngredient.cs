﻿using System;
using Inventory.Data;
using UnityEngine;

namespace Crafting.Data
{
    [Serializable]
    public class CraftIngredient
    {
        [SerializeField] private ItemConfigSO itemConfigSO;
        [field: SerializeField] public int Quantity { get; private set; }
        public int Id => itemConfigSO.Id;
    }
}