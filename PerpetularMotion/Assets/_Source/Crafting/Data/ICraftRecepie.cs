﻿using Identity;
using Inventory.Data;

namespace Crafting.Data
{
    public interface ICraftRecipe<T> where T : IItem, IIdentity
    {
        CraftIngredient[] Ingredients { get; }
        ItemConfigSO Result { get; }
    }
}