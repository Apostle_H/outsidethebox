﻿using System;
using Deck.Card;

namespace Deck.Hand
{
    public interface IHand<T>
    {
        int DefaultHandSize { get; }
        int CardsCount { get; }

        event Action<T> OnDraw;
        event Action<T> OnAdd;
        event Action<T> OnPlayed;
        event Action<T> OnDiscard;
        event Action<T> OnLost;

        void Draw();
        void Add(T card);
        void Play(int cardIndex);
        void Recover(T card);
    }

    public interface IHand<T, TInput>
    {
        int DefaultHandSize { get; }
        int CardsCount { get; }

        event Action<T> OnDraw;
        event Action<T> OnAdd;
        event Action<T> OnPlayed;
        event Action<T> OnDiscard;
        event Action<T> OnLost;

        void Draw();
        void Add(T card);
        void Play(int cardIndex, TInput input);
        void Recover(T card);
    }
}