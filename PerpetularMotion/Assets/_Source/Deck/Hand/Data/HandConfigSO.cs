﻿using UnityEngine;

namespace Deck.Hand.Data
{
    [CreateAssetMenu(menuName = "SO/Deck/Hand/ConfigSO", fileName = "NewHandConfigSO")]
    public class HandConfigSO : ScriptableObject
    {
        [field: SerializeField] public int Size { get; private set; }
    }
}