﻿using System.Collections.Generic;
using Deck.Card;

namespace Deck
{
    public interface IDeck<T>
    {
        List<T> GetDrawPile();
        List<T> GetDiscardPile();
        List<T> GetLostPile();
        
        bool Draw(out T card);
        void Discard(T card);
        void Lost(T card);
        void Shuffle();
        bool Recover(T card);
    }
}