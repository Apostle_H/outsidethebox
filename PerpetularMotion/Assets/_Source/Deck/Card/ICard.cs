﻿using Effect.Data;

namespace Deck.Card
{
    public interface ICard : IEffect
    {
        string Title { get; }
        bool IsLost { get; }
        
        string GetInfo();
    }

    public interface ICard<T> : IEffect<T>
    {
        string Title { get; }
        bool IsLost { get; }

        string GetInfo();
    }
}