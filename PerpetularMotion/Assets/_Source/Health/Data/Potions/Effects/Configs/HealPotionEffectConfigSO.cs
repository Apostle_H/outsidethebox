﻿using Crafting.Potion.Data;
using Crafting.Potion.Data.Effects;
using Effect.Data;
using Health.Data.Potions.Effects.Runtime;

namespace Health.Data.Potions.Effects.Configs
{
    public class HealPotionEffectConfigSO : PotionEffectConfigSO
    {
        public override IEffect Build()
        {
            return new HealPotionEffect();
        }
    }
}