﻿using System;

namespace Health.Data
{
    public interface IHealth
    {
        int Points { get; }

        public event Action<int> OnChanged;
        
        void Modify(int delta);
    }
}