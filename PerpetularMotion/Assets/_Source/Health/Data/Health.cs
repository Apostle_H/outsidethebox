﻿using System;

namespace Health.Data
{
    public class Health : IHealth
    {
        public int Points { get; private set; }
        
        public event Action<int> OnChanged;

        public Health(int points) => Points = points;

        public void Modify(int delta)
        {
            Points += delta;
        }
    }
}