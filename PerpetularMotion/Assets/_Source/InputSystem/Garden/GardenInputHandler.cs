﻿using System;
using VContainer;
using VContainer.Unity;

namespace InputSystem.Garden
{
    public class GardenInputHandler : IInitializable, IDisposable
    {
        private GardenActions _actions;

        [Inject]
        public GardenInputHandler(GardenActions actions) => _actions = actions;

        public void Initialize() => _actions.Enable();

        public void Dispose() => _actions.Disable();
    }
}