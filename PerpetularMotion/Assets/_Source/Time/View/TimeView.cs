﻿using System;
using Time.StateMachine;
using Time.StateMachine.States;
using UnityEngine.UIElements;
using VContainer;
using VContainer.Unity;

namespace Time.View
{
    public class TimeView : IStartable, IDisposable
    {
        private UIDocument _canvas;

        private TimeStateMachine _stateMachine;

        private VisualElement _timeSkipButton;

        private Type _currentState;

        public VisualElement Root { get; private set; }
        
        [Inject]
        public TimeView(UIDocument canvas, TimeStateMachine stateMachine)
        {
            _canvas = canvas;
            _stateMachine = stateMachine;
        }

        public void Start()
        {
            Root = _canvas.rootVisualElement.Q<VisualElement>("TimePanel");

            _timeSkipButton = Root.Q<VisualElement>("TimeSkipButton");
            
            Bind();
        }

        public void Dispose() => Expose();

        private void Bind() => _timeSkipButton.RegisterCallback<MouseDownEvent>(ToNextWeek);

        private void Expose() => _timeSkipButton.UnregisterCallback<MouseDownEvent>(ToNextWeek);

        private void ToNextWeek(MouseDownEvent evt) => _stateMachine.Switch<WeekendState>();
    }
}