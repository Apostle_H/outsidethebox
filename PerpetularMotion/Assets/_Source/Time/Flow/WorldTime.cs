﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Time.Flow
{
    public class WorldTime : ITimeUpdater
    {
        private List<ITimeUpdateable> _timeUpdateables = new();
        
        public int Time { get; private set; }
        public event Action<int> OnUpdateTime;

        public void UpdateTime(int delta)
        {
            Time += delta;

            foreach (var timeUpdateable in _timeUpdateables)
                timeUpdateable.UpdateTime(delta);
            
            OnUpdateTime?.Invoke(delta);
        }

        public void AddUpdateable(ITimeUpdateable updateable)
        {
            if (updateable == default)
                return;
            
            _timeUpdateables.Add(updateable);
        }

        public void RemoveUpdateable(ITimeUpdateable updateable)
        {
            if (updateable == default)
                return;

            _timeUpdateables.Remove(updateable);
        }
    }
}