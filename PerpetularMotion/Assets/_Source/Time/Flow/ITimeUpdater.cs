﻿namespace Time.Flow
{
    public interface ITimeUpdater : ITimeUpdateable
    {
        void AddUpdateable(ITimeUpdateable updateable);
        void RemoveUpdateable(ITimeUpdateable updateable);
    }
}