﻿using System;

namespace Time.Flow
{
    public interface ITimeUpdateable
    {
        int Time { get; }

        event Action<int> OnUpdateTime;
        
        void UpdateTime(int delta);
    }
}