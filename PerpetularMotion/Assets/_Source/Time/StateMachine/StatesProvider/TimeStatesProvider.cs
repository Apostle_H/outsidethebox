﻿using System;
using System.Collections.Generic;
using System.Linq;
using StateMachine;
using StateMachine.States;
using Time.StateMachine.States;
using VContainer;

namespace Time.StateMachine.StatesProvider
{
    public class TimeStatesProvider : ITimeStatesProvider
    {
        public IState StartingState { get; }
        private Dictionary<Type, IState> _states = new();

        [Inject]
        public TimeStatesProvider(BusinessWeekState businessWeekState, WeekendState weekendState)
        {
            StartingState = businessWeekState;
            
            _states.Add(businessWeekState.GetType(), businessWeekState);
            _states.Add(weekendState.GetType(), weekendState);
        }
        
        public Dictionary<Type, IState> GetStates() => 
            _states.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
    }
}