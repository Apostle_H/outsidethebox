﻿using StateMachine;

namespace Time.StateMachine.StatesProvider
{
    public interface ITimeStatesProvider : IStatesProvider { }
}