﻿using System;
using System.Collections.Generic;
using StateMachine;
using StateMachine.States;
using Time.StateMachine.StatesProvider;
using VContainer;
using VContainer.Unity;

namespace Time.StateMachine
{
    public class TimeStateMachine : IStateMachine, ITickable
    {
        private Dictionary<Type, IState> _states;
        private IState _currentState;

        [Inject]
        public TimeStateMachine(ITimeStatesProvider timeStatesProvider)
        {
            _currentState = timeStatesProvider.StartingState;
            
            _states = timeStatesProvider.GetStates();
            foreach (var kvp in _states)
                kvp.Value.Owner = this;
        }
        
        public void PostStart() => _currentState.Enter();

        public void Switch<T>() where T : IState
        {
            var nextState = typeof(T);
            if (!_states.ContainsKey(nextState))
                return;
            
            _currentState?.Exit();
            _currentState = _states[nextState];
            _currentState.Enter();
        }

        public void Tick() => _currentState?.Update();
    }
}