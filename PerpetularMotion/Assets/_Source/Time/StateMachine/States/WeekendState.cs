﻿using StateMachine;
using StateMachine.States;
using Time.Flow;
using VContainer;

namespace Time.StateMachine.States
{
    public class WeekendState : IState
    {
        private ITimeUpdater _timeUpdater;
        
        public IStateMachine Owner { get; set; }

        [Inject]
        public WeekendState(ITimeUpdater timeUpdater) => _timeUpdater = timeUpdater;

        public void Enter() => ToBusinessWeek();

        public void Update() { }

        public void Exit() => _timeUpdater.UpdateTime(1);

        private void ToBusinessWeek() => Owner.Switch<BusinessWeekState>();
    }
}