﻿using Deck.Hand;
using Garden.Deck.StateMachine;
using Garden.Deck.StateMachine.States;
using Garden.Field.StateMachine;
using Garden.Field.StateMachine.States;
using StateMachine;
using StateMachine.States;

namespace Time.StateMachine.States
{
    public class BusinessWeekState : IState
    {
        private GardenDeckStateMachine _gardenDeckStateMachine;
        private GardenFieldStateMachine _gardenFieldStateMachine;
        
        public IStateMachine Owner { get; set; }
        
        public BusinessWeekState(GardenDeckStateMachine gardenDeckStateMachine, GardenFieldStateMachine gardenFieldStateMachine)
        {
            _gardenDeckStateMachine = gardenDeckStateMachine;
            _gardenFieldStateMachine = gardenFieldStateMachine;
        }

        public void Enter()
        {
            _gardenFieldStateMachine.Switch<GardenFieldCalculateWeatherState>();
            _gardenDeckStateMachine.Switch<GardenDeckNextRoundState>();
        }

        public void Update() { }

        public void Exit() { }
    }
}