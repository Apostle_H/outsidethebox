﻿using UnityEngine;
using UnityEngine.UIElements;
using Utils.Extensions;
using VContainer;
using VContainer.Unity;

namespace View
{
    public class TopView : VisualElement, IPostStartable
    {
        private UIDocument _canvas;
        
        [Inject]
        public TopView(UIDocument canvas)
        {
            _canvas = canvas;
            
            name = "TopView";

            style.borderTopColor = new Color(0, 0, 0, 0);
            style.borderBottomColor = new Color(0, 0, 0, 0);
            style.borderLeftColor = new Color(0, 0, 0, 0);
            style.borderRightColor = new Color(0, 0, 0, 0);

            pickingMode = PickingMode.Ignore;
        }

        public void PostStart() => _canvas.rootVisualElement.Add(this);
    }
}