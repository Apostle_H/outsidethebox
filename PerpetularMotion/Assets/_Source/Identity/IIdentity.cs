﻿namespace Identity
{
    public interface IIdentity
    {
        int Id { get; }
    }
}