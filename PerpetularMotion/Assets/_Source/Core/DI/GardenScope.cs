﻿using Core.StateMachine;
using Core.StateMachine.States;
using Core.StateMachine.StatesProvider;
using Crafting.Potion;
using Crafting.Potion.Data;
using Crafting.Potion.View;
using Crafting.View;
using Crafting.View.StatesProvider;
using Deck;
using Deck.Hand;
using Deck.Hand.Data;
using DragNDropVE;
using Field.Grid;
using Field.Grid.Selector;
using Garden.Deck;
using Garden.Deck.Card;
using Garden.Deck.Data;
using Garden.Deck.StateMachine;
using Garden.Deck.StateMachine.States;
using Garden.Deck.StateMachine.StatesProvider;
using Garden.Deck.View;
using Garden.Deck.View.Data;
using Garden.Field.Grid;
using Garden.Field.Grid.Data;
using Garden.Field.StateMachine;
using Garden.Field.StateMachine.States;
using Garden.Field.StateMachine.StatesProvider;
using Garden.Field.Tile;
using Garden.Field.View.Data;
using Garden.Field.View.Grid;
using Garden.Field.View.Tile.Entity;
using Garden.Field.View.Tile.Entity.Pooling;
using Garden.Field.View.Tile.State;
using Garden.Field.Weather;
using InputSystem.Garden;
using Inventory;
using Inventory.View;
using Inventory.View.Data;
using StateMachine.States;
using Time.Flow;
using Time.StateMachine;
using Time.StateMachine.States;
using Time.StateMachine.StatesProvider;
using Time.View;
using UnityEngine;
using UnityEngine.InputSystem.UI;
using UnityEngine.UIElements;
using Utils.Pooling;
using Utils.Pooling.Data;
using Utils.Services;
using VContainer;
using VContainer.Unity;
using View;

namespace Core.DI
{
    public class GardenScope : LifetimeScope
    {
        [Header("Field")]
        [SerializeField] private Transform tileEntitiesTransformHolder;
        [SerializeField] private GardenGridConfigSO gridConfigSO;
        [SerializeField] private GardenGridViewConfigSO gridViewConfigSO;
        [SerializeField] private GardenTileStateViewConfigSO tileStateViewConfigSO;

        [Header("Deck")] 
        [SerializeField] private GardenDeckConfigSO deckConfigSO;
        [SerializeField] private GardenDeckViewConfigSO deckViewConfigSO;
        [SerializeField] private GardenCardPileViewConfigSO cardPileViewConfigSO;
        [SerializeField] private HandConfigSO handConfigSO;
        [SerializeField] private GardenHandViewConfigSO handViewConfigSO;

        [Header("Crafting")] 
        [SerializeField] private PotionMachineRecipesSO potionMachineRecipesSO;
        
        protected override void Configure(IContainerBuilder builder)
        {
            ConfigureCore(builder);
            ConfigureInput(builder);
            ConfigureUI(builder);
            ConfigureServices(builder);
            ConfigureView(builder);
            ConfigureDragNDrop(builder);
            ConfigureStateMachine(builder);
            ConfigureGardenField(builder);
            ConfigureGardenDeck(builder);
            ConfigureTime(builder);
            ConfigureInventory(builder);
            ConfigureCrafting(builder);
        }
        
        private void ConfigureCore(IContainerBuilder builder)
        {
            builder.Register<InGardenState>(Lifetime.Singleton);
            builder.Register<ICoreStatesProvider, CoreStatesProvider>(Lifetime.Singleton);
            
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<CoreStateMachine>();
            });
        }
        
        private void ConfigureInput(IContainerBuilder builder)
        {
            var gardenInput = new GardenActions();
            builder.RegisterInstance(gardenInput);

            builder.RegisterComponentInHierarchy<InputSystemUIInputModule>();
            
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<GardenInputHandler>();
            });
        }

        private void ConfigureUI(IContainerBuilder builder)
        {
            builder.RegisterComponentInHierarchy<UIDocument>();
        }

        private void ConfigureServices(IContainerBuilder builder)
        {
            builder.Register<UIService>(Lifetime.Singleton);
        }

        private void ConfigureView(IContainerBuilder builder)
        {
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<TopView>().AsSelf();
            });
        }

        private void ConfigureDragNDrop(IContainerBuilder builder)
        {
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<DraggerNDropper>();
            });
        }

        private void ConfigureStateMachine(IContainerBuilder builder)
        {
            builder.Register<WaitingState>(Lifetime.Singleton);
        }

        private void ConfigureGardenField(IContainerBuilder builder)
        {
            builder.RegisterInstance(gridConfigSO);
            builder.RegisterInstance(gridViewConfigSO);
            builder.RegisterInstance(tileStateViewConfigSO);
            
            builder.RegisterComponentInHierarchy<GardenGridView>();

            builder.Register<ISortPoolConfig<TileEntityView, int>, TileEntitiesViewsPoolConfig>(Lifetime.Singleton)
                .WithParameter(tileEntitiesTransformHolder);
            builder.Register<ISortPool<TileEntityView, int>, DefaultSortPool<TileEntityView, int>>(Lifetime.Singleton);

            builder.Register<GardenGridBuilder>(Lifetime.Singleton);
            builder.Register<IGridTileSelector, GridTileSelector>(Lifetime.Singleton);

            builder.Register<WeatherReport>(Lifetime.Singleton);
            builder.Register<WeatherResolver>(Lifetime.Singleton);

            builder.Register<GardenFieldInitializeState>(Lifetime.Singleton);
            builder.Register<GardenFieldCalculateWeatherState>(Lifetime.Singleton);
            builder.Register<GardenFieldGridUpdateState>(Lifetime.Singleton);
            builder.Register<IGardenFieldStatesProvider, GardenFieldStatesProvider>(Lifetime.Singleton);
            
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<GridTileSelectorView>();
                entryPoints.Add<TileStateView>();

                entryPoints.Add<GardenFieldStateMachine>().AsSelf();
                entryPoints.Add<GardenGrid>().As<IGrid<IGardenTile>>();
            });
        }

        private void ConfigureGardenDeck(IContainerBuilder builder)
        {
            builder.RegisterInstance(deckConfigSO);
            builder.RegisterInstance(deckViewConfigSO);
            builder.RegisterInstance(cardPileViewConfigSO);
            builder.RegisterInstance(handConfigSO);
            builder.RegisterInstance(handViewConfigSO);

            builder.Register<GardenDeckBuilder>(Lifetime.Singleton);
            
            builder.Register<GardenDeckInitializeState>(Lifetime.Singleton);
            builder.Register<GardenDeckChooseCardState>(Lifetime.Singleton);
            builder.Register<GardenDeckNextRoundState>(Lifetime.Singleton);
            builder.Register<IGardenDeckStatesProvider, GardenDeckStatesProvider>(Lifetime.Singleton);
            
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<GardenDeck>().As<IDeck<IGardenCard>>();
                entryPoints.Add<GardenHand>().As<IHand<IGardenCard, Vector2Int>>();

                entryPoints.Add<GardenDeckView>();
                entryPoints.Add<HandView>();
                
                entryPoints.Add<GardenDeckStateMachine>().AsSelf();
            });
        }

        private void ConfigureTime(IContainerBuilder builder)
        {
            builder.Register<ITimeUpdater, WorldTime>(Lifetime.Singleton);
            
            builder.Register<BusinessWeekState>(Lifetime.Singleton);
            builder.Register<WeekendState>(Lifetime.Singleton);
            builder.Register<ITimeStatesProvider, TimeStatesProvider>(Lifetime.Singleton);
            
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<TimeView>();
                
                entryPoints.Add<TimeStateMachine>().AsSelf();
            });
        }

        private void ConfigureInventory(IContainerBuilder builder)
        {
            builder.Register<IStorage, Storage>(Lifetime.Singleton);

            builder.Register<IPoolConfig<ItemSlotView>, ItemViewsPoolConfig>(Lifetime.Singleton);
            builder.Register<DefaultPool<ItemSlotView>>(Lifetime.Singleton);
            
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<InventoryView>().AsSelf();
            });
        }

        private void ConfigureCrafting(IContainerBuilder builder)
        {
            builder.RegisterInstance(potionMachineRecipesSO);
            
            builder.Register<ICraftingViewStatesProvider, CraftingViewStatesProvider>(Lifetime.Singleton);

            builder.Register<IPotionMachine, MainPotionMachine>(Lifetime.Singleton);
            builder.Register<PotionView>(Lifetime.Singleton);
            
            builder.UseEntryPoints(entryPoints =>
            {
                entryPoints.Add<CraftingView>().AsSelf();
            });
        }
    }
}