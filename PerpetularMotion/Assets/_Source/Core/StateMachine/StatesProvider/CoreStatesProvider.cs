﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.StateMachine.States;
using StateMachine;
using StateMachine.States;
using VContainer;

namespace Core.StateMachine.StatesProvider
{
    public class CoreStatesProvider : ICoreStatesProvider
    {
        public IState StartingState { get; }

        private readonly Dictionary<Type, IState> _states = new();

        [Inject]
        public CoreStatesProvider(InGardenState inGarden)
        {
            StartingState = inGarden;
            
            _states.Add(inGarden.GetType(), inGarden);
        }
        
        public Dictionary<Type, IState> GetStates() => 
            _states.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
    }
}