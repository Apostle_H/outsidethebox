﻿using StateMachine;

namespace Core.StateMachine.StatesProvider
{
    public interface ICoreStatesProvider : IStatesProvider { }
}