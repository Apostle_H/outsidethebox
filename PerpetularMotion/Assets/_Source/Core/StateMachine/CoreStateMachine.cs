﻿using System;
using System.Collections.Generic;
using Core.StateMachine.StatesProvider;
using StateMachine;
using StateMachine.States;
using VContainer;
using VContainer.Unity;

namespace Core.StateMachine
{
    public class CoreStateMachine : IStateMachine, IPostStartable, ITickable
    {
        private readonly Dictionary<Type, IState> _states;
        private IState _currentState;
        
        [Inject]
        public CoreStateMachine(ICoreStatesProvider coreStatesProvider)
        {
            _currentState = coreStatesProvider.StartingState;
            _states = coreStatesProvider.GetStates();

            foreach (var kpv in _states)
                kpv.Value.Owner = this;
        }

        public void PostStart() => _currentState.Enter();

        public void Switch<T>() where T : IState
        {
            var nextState = typeof(T);
            if (!_states.ContainsKey(nextState))
                return;
            
            _currentState?.Exit();
            _currentState = _states[nextState];
            _currentState.Enter();
        }

        public void Tick() => _currentState?.Update();
    }
}