﻿using Garden.Deck.StateMachine;
using Garden.Deck.StateMachine.States;
using Garden.Field.StateMachine;
using Garden.Field.StateMachine.States;
using StateMachine;
using StateMachine.States;
using VContainer;

namespace Core.StateMachine.States
{
    public class InGardenState : IState
    {
        private readonly GardenFieldStateMachine _gardenFieldStateMachine;
        private readonly GardenDeckStateMachine _gardenDeckStateMachine;
        public IStateMachine Owner { get; set; }
        
        [Inject]
        public InGardenState(GardenFieldStateMachine gardenFieldStateMachine, 
            GardenDeckStateMachine gardenDeckStateMachine)
        {
            _gardenFieldStateMachine = gardenFieldStateMachine;
            _gardenDeckStateMachine = gardenDeckStateMachine;
        }
        
        public void Enter()
        {
            _gardenFieldStateMachine.Switch<GardenFieldInitializeState>();
            _gardenDeckStateMachine.Switch<GardenDeckInitializeState>();
        }

        public void Update()
        {
            
        }

        public void Exit()
        {
            
        }
    }
}