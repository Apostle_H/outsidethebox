﻿using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Utils.CustomAttributes;
using Utils.Extensions;

namespace Editor.CustomPropertyDrawers
{
    [CustomPropertyDrawer(typeof(WorldVectorAttribute))]
    public class WorldVectorDrawer : PropertyDrawer
    {
        private const string SHOW_GIZMOS_BTN_TEXT = "Show";
        private const string HIDE_GIZMOS_BTN_TEXT = "Hide";

        private MonoBehaviour _targetObject;
        private SerializedProperty _targetProperty;
        
        private bool _isVector3;
        
        private Button _toggleHandlesBtn;
        private bool _showHandles;

        private bool _isBind;
        
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            _isVector3 = fieldInfo.FieldType == typeof(Vector3);
            
            if (!_isVector3)
                return base.CreatePropertyGUI(property);

            _targetObject = property.serializedObject.targetObject as MonoBehaviour;
            _targetProperty = property;
            
            var propertyBox = new Box();
            
            var propertyField = new PropertyField(property, fieldInfo.Name.CamelCaseToUpperCaseWithSpaces());

            _toggleHandlesBtn = new Button() { text = _showHandles ? HIDE_GIZMOS_BTN_TEXT : SHOW_GIZMOS_BTN_TEXT };
            
            propertyBox.Add(propertyField);
            propertyBox.Add(_toggleHandlesBtn);
            
            Bind();
            return propertyBox;
        }

        private void Bind()
        {
            if (_isBind)
                return;
            
            _toggleHandlesBtn.clicked += ToggleHandles;
            SceneView.duringSceneGui += DrawHandles;

            Selection.selectionChanged += Expose;

            _isBind = true;
        }

        private void Expose()
        {
            if (!_isBind || (Selection.gameObjects.Length == 1 && Selection.gameObjects[0] == _targetObject.gameObject))
                return;
            
            _toggleHandlesBtn.clicked -= ToggleHandles;
            SceneView.duringSceneGui -= DrawHandles;

            Selection.selectionChanged -= Expose;

            _isBind = false;
        }

        private void ToggleHandles()
        {
            _showHandles = !_showHandles;
            _toggleHandlesBtn.text = _showHandles ? HIDE_GIZMOS_BTN_TEXT : SHOW_GIZMOS_BTN_TEXT;
        }

        private void DrawHandles(SceneView sceneView)
        {
            if (!_showHandles)
                return;
            
            var offset = _targetProperty.vector3Value;
            var objectPosition = _targetObject.transform.position;
            var handlePosition = objectPosition + offset;

            var newHandleXZPosition = Handles.Slider2D(handlePosition, Vector3.up, 
                Vector3.forward, Vector3.right, .2f, Handles.CircleHandleCap, .1f);
            var newHandleYPosition = Handles.Slider(handlePosition, Vector3.up, .1f,
                Handles.ConeHandleCap, .1f);
            var newHandlePosition = newHandleXZPosition.ReplaceY(newHandleYPosition);
            
            _targetProperty.vector3Value = newHandlePosition - objectPosition;
            _targetProperty.serializedObject.ApplyModifiedProperties();
        }
    }
}