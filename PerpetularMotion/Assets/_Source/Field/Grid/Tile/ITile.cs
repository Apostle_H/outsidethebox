﻿using System;
using Field.Grid.Tile.TilesEntity;
using Time.Flow;
using UnityEngine;

namespace Field.Grid.Tile
{
    public interface ITile<T> : ITimeUpdateable where T : ITile<T>
    {
        IGrid<T> Owner { get; }
        Vector2Int GridIndex { get; }
        ITileEntity<T> Entity { get; }
        
        event Action<ITileEntity<T>> OnInstalledEntity;
        event Action<ITileEntity<T>> OnUninstalledEntity;
        
        void InstallEntity(ITileEntity<T> tileEntity);
        void UninstallEntity();
    }
}