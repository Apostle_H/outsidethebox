﻿using Time.Flow;

namespace Field.Grid.Tile.TilesEntity
{
    public interface ITileEntity<T> : ITimeUpdateable where T : ITile<T>
    {
        int TypeId { get; }
        
        T InstalledTile { get; }
        int LiveTime { get; }
        
        void SetInstalledTile(T tile);
        void Harvest();
        string GetInfo();
    }
}