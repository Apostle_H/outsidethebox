﻿using Identity;
using UnityEngine;

namespace Field.Grid.Tile.TilesEntity.Data
{
    public abstract class EntityConfigSO : ScriptableObject, IIdentity
    {
        public int Id => GetInstanceID();
    }
}