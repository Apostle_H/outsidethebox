﻿using System;
using UnityEngine;

namespace Field.Grid.Selector
{
    public class GridTileSelector : IGridTileSelector
    {
        public Vector2Int SelectedTileIndex { get; private set; }
        
        public event Action<Vector2Int> OnTileIndexSelected;

        public void SelectIndex(Vector2Int index)
        {
            SelectedTileIndex = index;
            
            OnTileIndexSelected?.Invoke(SelectedTileIndex);
        }
    }
}