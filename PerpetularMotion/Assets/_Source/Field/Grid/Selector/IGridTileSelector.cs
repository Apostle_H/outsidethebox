﻿using System;
using UnityEngine;

namespace Field.Grid.Selector
{
    public interface IGridTileSelector
    {
        public Vector2Int SelectedTileIndex { get; }

        public event Action<Vector2Int> OnTileIndexSelected; 
        
        public void SelectIndex(Vector2Int index);
    }
}