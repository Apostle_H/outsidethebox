﻿using System;
using Field.Grid.Tile;
using UnityEngine;

namespace Field.Grid
{
    public interface IGrid<T> where T : ITile<T>
    {
        event Action<T> OnTileAdded;

        void AddTile(T tile, Vector2Int index);
        
        bool GetTile(Vector2Int index, out T tile);

        void ForEachTile(Action<T> action);
    }
}